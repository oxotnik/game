package com.itacademy.gameexample;

import org.libsdl.app.SDLActivity;

public class MainActivity extends SDLActivity {
    @Override
    final protected String[] getLibraries() {
        return new String[]{"Game"};
    }

    @Override
    final protected String getMainFunction() {
        return "main";
    }
}
