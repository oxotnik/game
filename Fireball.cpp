//
// Created by Андрей Марцинкевич on 2.12.21.
//

#include "Fireball.hpp"
#include <Engine.hpp>
#include <Window.hpp>
#include <ParticleEmitter.hpp>

#include <glm/gtx/vector_angle.hpp>

Fireball::Fireball(const Engine& engine, const glm::vec2 &speed)
        : Node(engine)
        , _speed(speed)
{

    _sprite = engine.createShared<Sprite>("ball.png");
    _sprite->setRenderMask(0x1);

    _sprite->setScale(glm::vec2{0.1});
    this->addNode(_sprite);

    auto emitter = engine.createShared<ParticleEmitter>();
    emitter->start(5000, 0.0002f, 1.0);
    emitter->setRenderMask(0x1);
    this->addNode(emitter);

    this->scheduleUpdate();

}

void Fireball::update(fseconds delta)
{
    auto screedCord = (glm::inverse(_engine.getCamera()->getTransform()) * getTransform() * glm::vec3(1.0)).xy() /
                      _engine.getVirtualResolution();
    screedCord.y = 1.0f - screedCord.y;
    _engine.renderer().distortCoord.x = screedCord.x;
    _engine.renderer().distortCoord.y = screedCord.y;
    _engine.renderer().distortCoord.z = _engine.getCamera()->getScale().x;

    _totalTime += delta.count();

    if (_totalTime > 1.0)
    {
        _sprite->setScale(0.1f * (1.0f - glm::vec2(_totalTime - 1.0f) / 1.5f));
    }

    if (_totalTime >= 2.5)
    {
        this->removeFromParent();
        return;
    }
    _position += delta.count() * _speed * 1000.0f;
    _rotation =  glm::degrees(glm::orientedAngle(glm::vec2(1.0f, 0.0f), glm::normalize(_speed)));
    _transform = std::nullopt;
}
