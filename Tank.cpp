//
// Created by Андрей Марцинкевич on 2.12.21.
//

#include "Tank.hpp"

#include <Engine.hpp>
#include <Sprite.hpp>
#include <Sound.hpp>
#include <SoundManager.hpp>
#include <Utils/CppExt.hpp>
#include <glm/gtx/vector_angle.hpp>
#include "Fireball.hpp"
#include "ParticleEmitter.hpp"


Tank::Tank(Engine& engine)
    : Node(engine)
{
    _shootSound = engine.soundManager().createSound("shoot.wav", false);

    _body = engine.createShared<Sprite>("tank.png");
    _body->setPosition(_body->getContentSize() * 0.5f);

    _barrel = engine.createShared<Sprite>("pushka.png");
    _barrel->setAnchor({0.242f, 0.5f});
    _barrel->setPosition(_body->getContentSize() * 0.5f);

    _body->addNode(_barrel);
    this->addNode(_body);

    _contentSize = _body->getContentSize();


    _eventHandler += [this, &engine](const KeyEvent& e)
    {
        if (e.key == KeyCode::A)
        {
            _isBarrelLeft = (e.type == KeyEvent::Type::KeyDown);
        }

        if (e.key == KeyCode::D)
        {
            _isBarrelRight = (e.type == KeyEvent::Type::KeyDown);
        }

        if (e.key == KeyCode::LEFT)
        {
            _isLeft = (e.type == KeyEvent::Type::KeyDown);
        }

        if (e.key == KeyCode::RIGHT)
        {
            _isRight = (e.type == KeyEvent::Type::KeyDown);
        }

        if (e.key == KeyCode::UP)
        {
            _isUp = (e.type == KeyEvent::Type::KeyDown);
        }

        if (e.key == KeyCode::DOWN)
        {
            _isDown = (e.type == KeyEvent::Type::KeyDown);
        }

        if (e.key == KeyCode::SPACE && e.type == KeyEvent::Type::KeyUp)
        {
            _shootSound->stop();
            _shootSound->play();

            auto vector = glm::rotate(glm::vec2{1.0f, 0.0f}, glm::radians(getRotation() + _barrel->getRotation()));

            auto vector2 = glm::rotate(glm::vec2{1.0f, 0.0f}, glm::radians(getRotation()));

            auto fireball = engine.createShared<Fireball>(vector + vector2 * _speed  / 1500.0f);
            fireball->setPosition(_position + vector * (160.0f * 0.4f));
            fireball->setScale(glm::vec2{0.4});
            engine.scene()->addNode(std::move(fireball));

//            if (emitter->getParent() == nullptr)
//            {
//                engine.scene()->addNode(emitter);
//            }
//
//            emitter->setPosition(_position + vector * 160.0f);
//            emitter->start(1000);

        }
    };

    this->scheduleUpdate();
}

void Tank::update(fseconds delta)
{
    constexpr auto barrelSpeed = 70.0f;

    if (_isBarrelLeft)
    {
        _barrel->setRotation(_barrel->getRotation() - delta.count() * barrelSpeed);
    }
    if (_isBarrelRight)
    {
        _barrel->setRotation(_barrel->getRotation() + delta.count() * barrelSpeed);
    }

    if (_isLeft)
    {
        if (std::abs(_turnSpeed) < 70.0f)
        {
            _turnSpeed += delta.count() * 120.0f + _speed * 0.002;
        }

        _transform = std::nullopt;
    }
    if (_isRight)
    {
        if (std::abs(_turnSpeed) < 70.0f)
        {
            _turnSpeed -= delta.count() * 120.0f + _speed * 0.002;
            _turnSpeed = std::min(_turnSpeed, 70.0f);
        }
    }

    if (_turnSpeed > 0.1f)
    {
        _rotation -= delta.count() * _turnSpeed;
        _turnSpeed -= delta.count() * 115.0f;
        _transform = std::nullopt;
        if (_turnSpeed <= 0.1)
        {
            _turnSpeed = 0.0f;
        }
    }


    if (_turnSpeed < -0.1f)
    {
        _rotation -= delta.count() * _turnSpeed;
        _turnSpeed += delta.count() * 115.0f;
        _transform = std::nullopt;
        if (_turnSpeed >= -0.1)
        {
            _turnSpeed = 0.0f;
        }
    }

    if (_isUp)
    {
        if (_speed < 500.0f)
        {
            _speed += delta.count() * 460.0f;
        }
    }

    if (_isDown)
    {
        if (_speed > -200.0f)
        {
            _speed -= delta.count() * 460.0f;
        }
    }

    auto vector = glm::rotate(glm::vec2{1.0f, 0.0f}, glm::radians(getRotation()));

    if (_speed > 0.1f)
    {
        _position += vector * delta.count() * _speed;
        _speed -= delta.count() * 400.0f;
        _transform = std::nullopt;

        if (_speed <= 0.1)
        {
            _speed = 0.0f;
        }
    }

    if (_speed < -0.1f)
    {
        _position += vector * delta.count() * _speed;
        _speed += delta.count() * 400.0f;
        _transform = std::nullopt;

        if (_speed > -0.1)
        {
            _speed = 0.0f;
        }
    }
}

float Tank::getSpeed() const
{
    return _speed;
}
