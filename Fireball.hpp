//
// Created by Андрей Марцинкевич on 2.12.21.
//

#ifndef GAME_FIREBALL_HPP
#define GAME_FIREBALL_HPP

#include <Sprite.hpp>

class Fireball : public Node
{
public:
    Fireball(const Engine& engine, const glm::vec2 &speed);

    void update(fseconds dt) override;

private:
    std::shared_ptr<Sprite> _sprite;
    glm::vec2 _speed;
    double _totalTime = 0.0f;
};


#endif //GAME_FIREBALL_HPP
