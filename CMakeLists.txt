cmake_minimum_required(VERSION 3.10.2)
project(Game)

set(CMAKE_CXX_STANDARD 17)

set(GAME_SRC main.cpp Tank.cpp Tank.hpp Fireball.cpp Fireball.hpp)

if(ANDROID)
    add_library(Game SHARED ${GAME_SRC})
else()
    add_executable(Game ${GAME_SRC})
endif()

add_subdirectory(Engine)

target_link_libraries(Game Engine)
