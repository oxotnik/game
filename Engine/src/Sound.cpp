//
// Created by Андрей Марцинкевич on 8.12.21.
//

#include "Sound.hpp"
#include "SoundManager.hpp"

#include <string>
#include <vector>

#include <SDL.h>

Sound::Sound(std::string_view filename, bool isLoop, const SoundParams& params)
{
    _isLoop = isLoop;

    SDL_RWops* file = SDL_RWFromFile(filename.data(), "rb");
    if (file == nullptr)
    {
        throw std::runtime_error(std::string(filename) + "doesn't exist");
    }

    SDL_AudioSpec audioSpecFromFile{};
    const int32_t auto_delete_file            = 1;
    uint8_t*      sample_buffer_from_file     = nullptr;
    uint32_t      sample_buffer_len_from_file = 0;

    SDL_AudioSpec* audio_spec = SDL_LoadWAV_RW(file,
                                               auto_delete_file,
                                               &audioSpecFromFile,
                                               &sample_buffer_from_file,
                                               &sample_buffer_len_from_file);

    if (audio_spec == nullptr)
    {
        throw std::runtime_error(std::string(filename) + " can't parse and load audio samples from file");
    }

    if (params.freq != audioSpecFromFile.freq)
    {
        throw std::runtime_error(std::string(filename) + " can't support freq");
    }

    if (params.channels != audioSpecFromFile.channels)
    {
        throw std::runtime_error(std::string(filename) + " can't support channels");
    }

    auto format = [&]()
    {
        switch (audioSpecFromFile.format)
        {
            case AUDIO_U8: return SoundParams::Format::U8;
            case AUDIO_S8: return SoundParams::Format::S8;
            case AUDIO_S16LSB: return SoundParams::Format::S16LSB;
            case AUDIO_U16LSB: return SoundParams::Format::U16LSB;
            case AUDIO_S16MSB: return SoundParams::Format::S16MSB;
            case AUDIO_U16MSB: return SoundParams::Format::U16MSB;
            default: return SoundParams::Format::Unknown;
        }
    }();

    if (params.format != format)
    {
        throw std::runtime_error(std::string(filename) + " can't support format");
    }

    _buffer.start = sample_buffer_from_file;
    _buffer.size = sample_buffer_len_from_file;
    _buffer.current_pos = 0;
}

void Sound::play()
{
    _state = State::Playing;
}

void Sound::pause()
{
    _state = State::Paused;
}

void Sound::stop()
{
    _state = State::Paused;
    _buffer.current_pos = 0;
}

bool Sound::isPlaying()
{
    return _state == State::Playing;;
}

bool Sound::isLoop()
{
    return _isLoop;
}

Sound::~Sound()
{
    SDL_FreeWAV(_buffer.start);
}

float Sound::getVolume() const
{
    return _volume;
}

void Sound::setVolume(float volume)
{
    _volume = volume;
}

