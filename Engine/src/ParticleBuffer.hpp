//
// Created by Андрей Марцинкевич on 22.12.21.
//

#ifndef GAME_PARTICLEBUFFER_HPP
#define GAME_PARTICLEBUFFER_HPP

#include <Utils/CppExt.hpp>
#include <VertexBuffer.hpp>

class ParticleBuffer : public VertexBuffer
{
public:
    struct ParticleData
    {
        glm::vec2 pos;
        glm::vec2 velocity;
        ext::color color = glm::vec4{1.0};
        float phase = 0;
    };

    virtual void addData(ParticleData data) = 0;

    virtual ~ParticleBuffer() = default;
};


#endif //GAME_PARTICLEBUFFER_HPP
