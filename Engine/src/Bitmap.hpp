//
// Created by Андрей Марцинкевич on 29.11.21.
//

#ifndef GAME_BITMAP_HPP
#define GAME_BITMAP_HPP

#include <string_view>
#include <Utils/CppExt.hpp>
#include <glm/glm.hpp>

class Engine;
class Bitmap
{
public:
    enum class Format
    {
        R,
        Rgb,
        Rgba,
        Rgba16F,
        Rgba32F,
        Rgb16F,
        Rgb32F,
    };

    Bitmap(const Engine& engine, std::string_view filepath);

    Bitmap(Format format, ext::array_view<unsigned char> image, glm::vec2 size);

    [[nodiscard]] const ext::array_view<unsigned char>& getImage() const;

    [[nodiscard]] Format getFormat() const;
    [[nodiscard]] glm::vec2 getSize() const;

private:
    Format _format;
    ext::array_view<unsigned char> _image;
    glm::vec2 _size;
};


#endif //GAME_BITMAP_HPP
