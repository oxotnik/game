//
// Created by Андрей Марцинкевич on 22.11.21.
//

#ifndef GAME_MESHDATA_HPP
#define GAME_MESHDATA_HPP

#include <cstddef>
#include <vector>
#include <glm/glm.hpp>
#include <Utils/CppExt.hpp>

class MeshData
{
public:
    struct Vertex
    {
        glm::vec2 position;
        glm::vec2 texcoord;
        ext::color color = glm::vec4{1.0, 1.0, 1.0, 1.0};
    };

    ext::array_view<Vertex> vertices;
    ext::array_view<uint32_t> indexes;
};

#endif //GAME_MESHDATA_HPP
