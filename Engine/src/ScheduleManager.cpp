//
// Created by Андрей Марцинкевич on 6.12.21.
//

#include "ScheduleManager.hpp"

#include <Utils/CppExt.hpp>

ScheduleManager::ScheduleManager()
{
    _prevTime = std::chrono::high_resolution_clock::now();
}

void ScheduleManager::update()
{
    if (_isFirstRun)
    {
        _prevTime = std::chrono::high_resolution_clock::now();
        _isFirstRun = false;
    }

    auto now = std::chrono::high_resolution_clock::now();
    _delta = now - _prevTime;
    _prevTime = now;

    _handlers.forEach([&](HandlerStorage& handler)
    {
        bool needRun = true;

        if (handler.delay.count() > 0.0f)
        {
            auto delta = now - handler.lastTime;
            if (delta >= handler.delay)
            {
                handler.delay = 0s;
            }
            else
            {
                needRun = false;
            }
        }

        if (handler.delta.count() > 0 && needRun && (now - handler.lastTime) < handler.delta)
        {
            needRun = false;
        }

        if (needRun)
        {
            handler.lastTime = now;
            handler.fun(_delta);
        }

        if (handler.count != 0)
        {
            --handler.count;

            if (handler.count == 0)
            {
                _handlers.removeIf([&](const HandlerStorage& curHandler)
                {
                   return curHandler.internalTag == handler.internalTag;
                });
            }
        }
    });
}

void ScheduleManager::scheduleUpdate(handler_fun fun, size_t tag) const
{
    this->schedule(std::move(fun), 0s, 0s, 0, tag);
}

void ScheduleManager::stop(size_t tag) const
{
    _handlers.removeIf([&](const HandlerStorage& handler)
    {
        return handler.tag == tag;
    });
}

fseconds ScheduleManager::getCurrentDelta() const
{
    return _delta;
}

void ScheduleManager::schedule(ScheduleManager::handler_fun fun,
                               fseconds delay,
                               fseconds delta,
                               size_t count,
                               size_t tag) const
{
    HandlerStorage handler;
    handler.fun = std::move(fun);
    handler.count = count;
    handler.delay = delay;
    handler.delta = delta;
    handler.tag = tag;
    handler.internalTag = ext::genUniqueObjectId();

    handler.lastTime = std::chrono::high_resolution_clock::now();

    _handlers.add(std::move(handler));
}
