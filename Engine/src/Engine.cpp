//
// Created by Андрей Марцинкевич on 17.11.21.
//

#include "Engine.hpp"
#include "Node.hpp"

#include <SDL/SdlWindow.hpp>
#include <SDL/SdlRenderer.hpp>

#include <EventsManager.hpp>
#include <ScheduleManager.hpp>
#include <SoundManager.hpp>
#include <ImguiManager.hpp>
#include <ActionManager.hpp>
#include <FileManager.hpp>

Engine::Engine()
{
    _fileManager = createUnique<FileManager>();
    _eventsManager = createUnique<EventsManager>();
    _scheduleManager = createUnique<ScheduleManager>();
    _actionManager = createUnique<ActionManager>();
    _scheduleManager->scheduleUpdate([this](fseconds dt)
    {
        _actionManager->update(dt);
    }, ext::genUniqueObjectId());
}

void Engine::init(std::string_view windows_name, size_t width, size_t height)
{
    _window = createUnique<SdlWindow>("Game",
                                      width, height,
                                          SdlWindow::RenderMode::OpenGl);

    _virtualResolution = glm::vec2(_window->getWidth(), _window->getHeight());


    _soundManager = createUnique<SoundManager>();

    _renderer = _window->createRenderer();
    _scene = createShared<Node>();
    _camera = createShared<Node>();

    _camera->setContentSize(_virtualResolution);


    EventsAutoRegistarator reg(*_eventsManager, EventsAutoRegistarator::NoRemove);
    reg += [this](const SystemEvent& e)
    {
        if (e.type == SystemEvent::Type::Quit)
        {
            _isActive = false;
        }
    };

    _renderer->init();

    reg += [this](const ResizeEvent& e)
    {
        _window->resize(e.width, e.height);
        _renderer->setViewport({0, 0}, {e.width, e.height});
    };

    _isActive = true;
}

bool Engine::isActive()
{
    return _isActive;
}

void Engine::update()
{
    _window->update();
    _scheduleManager->update();

    _scene->visit();
//    _imguiManager->visit();
    _renderer->draw();

    _window->swap();
}

const EventsManager &Engine::eventsManager() const
{
    return *_eventsManager;
}

const Renderer &Engine::renderer() const
{
    return *_renderer;
}

std::shared_ptr<Node> Engine::scene()
{
    return _scene;
}

const Window &Engine::window() const
{
    return *_window;
}

const ScheduleManager &Engine::scheduleManager() const
{
    return *_scheduleManager;
}

const SoundManager& Engine::soundManager() const
{
    return *_soundManager;
}

const ActionManager& Engine::actionManager() const
{
    return *_actionManager;
}

const FileManager& Engine::fileManager() const
{
    return *_fileManager;
}


glm::vec2 Engine::getVirtualResolution() const
{
    return _virtualResolution;
}

void Engine::setVirtualResolution(glm::vec2 virtualResolution) const
{
    _virtualResolution = virtualResolution;
    _camera->setContentSize(_virtualResolution);
}

std::shared_ptr<Node> Engine::getCamera() const
{
    return _camera;
}


Engine::~Engine() = default;
