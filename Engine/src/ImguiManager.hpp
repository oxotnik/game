//
// Created by Андрей Марцинкевич on 11.12.21.
//

#ifndef GAME_IMGUIMANAGER_HPP
#define GAME_IMGUIMANAGER_HPP

#include <memory>
#include <stack>
#include <Renderer.hpp>
#include <EventsManager.hpp>

class Texture;
class Engine;
class TextureUniform;
class Mat3Uniform;
class Vec2Uniform;

class MenuItem
{
public:
    virtual void visit() = 0;
};

class Button : public MenuItem
{
public:
    explicit Button(std::string text)
            : _text(std::move(text))
    {}

    void visit() override;

private:
    std::string _text;
};

class BeginItem : public MenuItem
{
public:
    explicit BeginItem(std::string text)
    : _text(std::move(text))
    {}

    void visit() override;

private:
    std::string _text;
};

class EndItem : public MenuItem
{
public:
    void visit() override;
};

class ImguiManager final
{
public:
    explicit ImguiManager(const Engine& engine);
    void visit();

//    void addMenuItem(std::shared_ptr<MenuItem> item);
//    void removeMenuItem(const std::shared_ptr<MenuItem>& item);

    //class
    using MenuItems = std::vector<std::shared_ptr<MenuItem>>;

    void push(std::shared_ptr<MenuItems> items);
    void pop();


private:
    const Engine& _engine;

    Renderer::Command _command;

    std::shared_ptr<Vec2Uniform> _screenSizeUniform;
    std::shared_ptr<Mat3Uniform>  _transformUniform;
    std::shared_ptr<TextureUniform>  _textureUniform;

    EventsAutoRegistarator _eventHandler;

    bool show_demo_window = true;

    bool _rMousePressed {false};
    bool _lMousePressed {false};
    bool _mMousePressed {false};

    glm::vec2 _mousePos;

    float _mouseWheel = 0.0f;

    std::stack<MenuItems> _uis;
};


#endif //GAME_IMGUIMANAGER_HPP
