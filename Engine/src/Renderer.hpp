//
// Created by Андрей Маsрцинкевич on 20.11.21.
//

#ifndef GAME_RENDERER_HPP
#define GAME_RENDERER_HPP

#include <vector>
#include <optional>
#include <string_view>

#include <VertexData.hpp>
#include <Bitmap.hpp>
#include <ParticleBuffer.hpp>

class VertexBuffer;
class ShaderProgram;
class Texture;
class Uniform;

class Renderer
{
public:
    virtual ~Renderer() = default;
    virtual void init() = 0;

    struct Command
    {
        uint32_t mask;
        std::shared_ptr<VertexBuffer>  vertexBuffer;
        std::shared_ptr<ShaderProgram> program;
        std::vector<std::shared_ptr<Uniform>> uniforms;

        std::optional<glm::vec4> scissor;

        struct sub_t
        {
            size_t num;
            size_t offset;
        };

        std::optional<sub_t> sub;
    };

    void addCommand(Command command) const
    {
        _commands.push_back(std::move(command));
    }

    virtual void draw() = 0;

    virtual std::shared_ptr<VertexBuffer> createVertexBuffer(const VertexData& data) const = 0;
    virtual std::shared_ptr<ParticleBuffer> createParticleBuffer(std::vector<ParticleBuffer::ParticleData> data) const = 0;
    virtual std::shared_ptr<ParticleBuffer> createParticleBuffer(size_t count) const = 0;


    virtual std::shared_ptr<ShaderProgram> createProgram(std::string_view name) const = 0;
    virtual std::shared_ptr<Texture> createTexture(Bitmap bitmap) const = 0;

    virtual void setViewport(glm::vec2 origin, glm::vec2 size) const = 0;


    mutable glm::vec3 distortCoord;

    virtual glm::vec2 getRenderResolution() const = 0;

protected:
    mutable std::vector<Command> _commands;
};

#endif //GAME_RENDERER_HPP
