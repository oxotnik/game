//
// Created by Андрей Марцинкевич on 3.01.22.
//

#ifndef GAME_FRAMEBUFFER_HPP
#define GAME_FRAMEBUFFER_HPP

#include <memory>
#include <functional>

class Texture;
class Framebuffer
{
public:
    virtual ~Framebuffer() = default;
    std::shared_ptr<Texture> getTexture() const;

    virtual void drawToFrameBuffer(std::function<void(void)> callback) = 0;

protected:
    std::shared_ptr<Texture> _texture;
};


#endif //GAME_FRAMEBUFFER_HPP
