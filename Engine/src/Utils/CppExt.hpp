    //
// Created by martsinkevich on 18.01.2018.
//

#pragma once

#include <tuple>
#include <type_traits>
#include <glm/glm.hpp>

#include <functional>

#define XSTRINGIFY(A) STRINGIFY(A)
#define STRINGIFY(A) #A

namespace ext
{
    namespace detail
    {
        template<int... Is>
        struct seq { };

        template<int N, int... Is>
        struct gen_seq : gen_seq<N - 1, N - 1, Is...> { };

        template<int... Is>
        struct gen_seq<0, Is...> : seq<Is...> { };

        template<typename T, typename F, int... Is>
        inline void for_each(T&& t, F f, seq<Is...>)
        {
            auto l = { (f(std::get<Is>(t)), 0)... };
        }


        template <typename T, typename = std::void_t<>>
        struct has_value_type : std::false_type {};

        template <typename T>
        struct has_value_type<T, std::void_t<typename T::value_type>> : std::true_type {};

        template <typename T>
        inline auto inner_type_helper(T&& t)
        {
            if constexpr (has_value_type<T>())
            {
                return std::declval<typename T::value_type>();
            }
            else
            {
                return *std::begin(t);
            }
        }
    }

    template<typename... Ts, typename F>
    inline void for_each(std::tuple<Ts...> const& t, F f)
    {
        detail::for_each(t, f, detail::gen_seq<sizeof...(Ts)>());
    }

    template <typename T>
    inline size_t objectId(const T& object)
    {
        return reinterpret_cast<size_t>(&object);
    }

    inline size_t genUniqueObjectId()
    {
        static size_t id = 0;
        return ++id;
    }

    template <typename T>
    inline size_t classId()
    {
        static int _id;
        return reinterpret_cast<size_t>(&_id);
    }

    template <typename T>
    using inner_type = decltype(detail::inner_type_helper(std::declval<T>()));

    template <typename T>
    struct function_traits
        : public function_traits<decltype(&T::operator())>
    {};

    template <typename ClassType, typename ReturnType, typename... Args>
    struct function_traits<ReturnType(ClassType::*)(Args...) const>
    {
        enum { arity = sizeof...(Args) };

        typedef ReturnType result_type;

        template <size_t i>
        struct arg
        {
            typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
        };
    };


    struct color
    {
        explicit color(std::uint32_t rgba) : _rgba(rgba) {}
        color(glm::vec4 color)
        {
            this->setColor(color);
        }

        glm::vec4 getColor() const
        {
            std::uint32_t r = (_rgba & 0x000000FF) >> 0;
            std::uint32_t g = (_rgba & 0x0000FF00) >> 8;
            std::uint32_t b = (_rgba & 0x00FF0000) >> 16;
            std::uint32_t a = (_rgba & 0xFF000000) >> a;

            return glm::vec4(r, g, b, a) / 255.0f;
        }

        void setColor(glm::vec4 color)
        {
            assert(color.r <= 1 && color.r >= 0);
            assert(color.g <= 1 && color.g >= 0);
            assert(color.b <= 1 && color.b >= 0);
            assert(color.a <= 1 && color.a >= 0);

            auto r = static_cast<std::uint32_t>(color.r * 255);
            auto g = static_cast<std::uint32_t>(color.g * 255);
            auto b = static_cast<std::uint32_t>(color.b * 255);
            auto a = static_cast<std::uint32_t>(color.a * 255);

            _rgba = a << 24 | b << 16 | g << 8 | r;
        }

        uint32_t getRgba() const
        {
            return _rgba;
        }

        void setRgba(uint32_t rgba)
        {
            _rgba = rgba;
        }

    private:
        std::uint32_t _rgba = 0;
    };

    template<typename T>
    class array_view final
    {
    public:
        explicit array_view() : _size(0) {};
        array_view(nullptr_t) : _size(0) {};

        array_view(array_view&& other) noexcept
        {
            *this = std::move(other);
        }

        array_view& operator=(array_view&& other) noexcept
        {
            _dataStorage = std::move(other._dataStorage);
            other._dataStorage = nullptr;

            _freeFun = std::move(other._freeFun);
            other._freeFun = nullptr;

            _size = other._size;
            other._size = 0;

            return *this;
        }

        ~array_view()
        {
            if (_freeFun)
            {
                _freeFun();
            }
        }

        array_view(const T* data, size_t size, std::function<void()> freeFun = nullptr)
         : _size(size)
        {
            _freeFun = std::move(freeFun);
            _dataStorage = [data]()
            {
                return data;
            };
        }

        template<typename Container>
        array_view(Container container)
        {
            *this = std::move(container);
        }


        template<typename Container>
        array_view& operator=(Container container)
        {
            _size = std::size(container);
            _dataStorage = [container = std::move(container)]()
            {
                return std::data(container);
            };

            return *this;
        }

        size_t get_size() const {return _size;};
        const T* get_data() const
        {
            return _dataStorage();
        }

    protected:
        size_t _size{0};
        std::function<const T*()> _dataStorage;
        std::function<void()> _freeFun;
    };
}
