//
// Created by Андрей Марцинкевич on 5.01.22.
//

#ifndef GAME_GLBLURPROGRAM_HPP
#define GAME_GLBLURPROGRAM_HPP


#include "GlProgram.hpp"

class GlBlurProgram final : public GlProgram
{
public:
    explicit GlBlurProgram();
};

#endif //GAME_GLBLURPROGRAM_HPP
