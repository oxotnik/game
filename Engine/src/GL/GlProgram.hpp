//
// Created by Андрей Марцинкевич on 23.11.21.
//

#ifndef GAME_GLPROGRAM_HPP
#define GAME_GLPROGRAM_HPP

#include <ShaderProgram.hpp>

#include <cstdint>
#include <string>
#include <glm/glm.hpp>

class GlProgram : public ShaderProgram
{
public:

    GlProgram(std::initializer_list<const char*> attributes, std::string vs, std::string ps);
    ~GlProgram() override;

    std::shared_ptr<TextureUniform> createTextureUniform(std::string_view name) override;
    std::shared_ptr<Mat3Uniform> createMat3Uniform(std::string_view name) override;
    std::shared_ptr<Vec2Uniform> createVec2Uniform(std::string_view name) override;
    std::shared_ptr<Vec3Uniform> createVec3Uniform(std::string_view name) override;
    std::shared_ptr<FloatUniform> createFloatUniform(std::string_view name) override;

    void activate() override;

    uint32_t getProgramId() const {return _program;}

protected:
    uint32_t _vertexShader;
    uint32_t _fragmentShader;
    uint32_t _program;
};

class GlTextureUniform : public TextureUniform
{
public:
    explicit GlTextureUniform(const std::shared_ptr<GlProgram>& program, std::string_view name);
    void activate() override;
    void activateWithSlot(size_t) override;

private:
    int32_t _location;
};

class GlMat3Uniform : public Mat3Uniform
{
public:
    explicit GlMat3Uniform(const std::shared_ptr<GlProgram>& program, std::string_view name);
    void activate() override;

private:
    int32_t _location;
};

class GlVec2Uniform : public Vec2Uniform
{
public:
    explicit GlVec2Uniform(const std::shared_ptr<GlProgram>& program, std::string_view name);
    void activate() override;

private:
    int32_t _location;
};

class GlVec3Uniform : public Vec3Uniform
{
public:
    explicit GlVec3Uniform(const std::shared_ptr<GlProgram>& program, std::string_view name);
    void activate() override;

private:
    int32_t _location;
};


class GlFloatUniform : public FloatUniform
{
public:
    explicit GlFloatUniform(const std::shared_ptr<GlProgram>& program, std::string_view name);
    void activate() override;

private:
    int32_t _location;
};


#endif //GAME_GLPROGRAM_HPP
