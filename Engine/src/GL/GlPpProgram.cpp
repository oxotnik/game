//
// Created by Андрей Марцинкевич on 3.01.22.
//

#include "GlPpProgram.hpp"

namespace
{
    constexpr auto vs_program =
            R"(
VS_IN vec2 position;

VS_OUT vec2 oTexCoord;

void main()
{
    oTexCoord = position * 0.5 + vec2(0.5);
    gl_Position = vec4(position.x, position.y, 1.0, 1.0);
}
)";
    constexpr auto ps_program =
            R"(
#version 330 core

uniform sampler2D uTexture;
uniform sampler2D uTexture2;

uniform vec3 distortCoord;
uniform float uTime;

VS_IN vec2 oTexCoord;

void main()
{
    vec2 distVec = normalize(distortCoord.xy - oTexCoord);
    float distFactor = length(distortCoord.xy - oTexCoord) * distortCoord.z;

    float distCoef = mix(1.0, 0.0, distFactor * 5.0) * mix(0.0, 1.0, distFactor * 5.0) * 4.0;

    if (distFactor < 0.2)
    {
        VS_OUT = TEXTURE2D(uTexture, oTexCoord + 1.0/distortCoord.z * distVec * sin((1.0 + cos(uTime * 4.0)) * 0.5 * distFactor * 80.0) * 0.01 * distCoef);
    }
    else
    {
        VS_OUT = TEXTURE2D(uTexture, oTexCoord);
    }

    VS_OUT += TEXTURE2D(uTexture2, oTexCoord) * 0.6;
}
)";
}

GlPpProgram::GlPpProgram()
        : GlProgram({"position"}, vs_program, ps_program)
{

}
