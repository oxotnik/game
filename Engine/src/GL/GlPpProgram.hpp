//
// Created by Андрей Марцинкевич on 3.01.22.
//

#ifndef GAME_GLPPPROGRAM_HPP
#define GAME_GLPPPROGRAM_HPP

#include <GL/GlProgram.hpp>

class GlPpProgram final : public GlProgram
{
public:
    GlPpProgram();
};


#endif //GAME_GLPPPROGRAM_HPP
