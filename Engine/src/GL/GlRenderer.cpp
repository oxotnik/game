//
// Created by Андрей Марцинкевич on 23.11.21.
//

#include "GlRenderer.hpp"
#include "GlVertexBuffer.hpp"
#include "GlSpriteProgram.hpp"
#include "GlParticleBuffer.hpp"
#include "GlParticleProgram.hpp"
#include "Framebuffer.hpp"
#include "GlPpProgram.hpp"
#include "GlFramebuffer.hpp"
#include "ScheduleManager.hpp"
#include "GlBlurProgram.hpp"

#include <Engine.hpp>
#include <Window.hpp>

#include <SDL.h>
#include <GL/GlHeaders.hpp>
#include <VertexDataHelper.hpp>

namespace
{
    struct SimpleVertex
    {
        glm::vec2 pos;

        SimpleVertex(glm::vec2 pos)
                : pos(pos)
        {}
    };

    using SimpleVertexData =
            VertexDataCreator<SimpleVertex,
                    LayoutGen<glm::vec2>,
                    OffsetsGen<offsetof(SimpleVertex, pos)>>;
}


GlRenderer::GlRenderer(const Engine &engine, SDL_Window *window)
        : _engine(engine)
        , _drawContext{SDL_GL_CreateContext(window), SDL_GL_DeleteContext}
{
    SDL_Log("OpenGL version supported by this platform (%s): \n",
           glGetString(GL_VERSION));

    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if defined __ANDROID__

#else
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
#endif
}

void GlRenderer::init()
{
    _ppProgram = std::make_shared<GlPpProgram>();
    _blurProgram = std::make_shared<GlBlurProgram>();

    _origFbo = std::make_shared<GlFramebuffer>(_engine, glm::vec2(_engine.window().getWidth(), _engine.window().getHeight()));
    _bloomFbo = std::make_shared<GlFramebuffer>(_engine, 0.25f * glm::vec2(_engine.window().getWidth(), _engine.window().getHeight()));
    _blurFbo = std::make_shared<GlFramebuffer>(_engine, 0.25f * glm::vec2(_engine.window().getWidth(), _engine.window().getHeight()));

    _blurTextureUniform = _blurProgram->createTextureUniform("uTexture");
    _blurRadUniform = _blurProgram->createVec2Uniform("uDir");

    _textureUniform = _ppProgram->createTextureUniform("uTexture");
    _texture2Uniform = _ppProgram->createTextureUniform("uTexture2");
    _distortUniform = _ppProgram->createVec3Uniform("distortCoord");
    _timeUniform = _ppProgram->createFloatUniform("uTime");

    _timeUniform->value = 0;
    _textureUniform->texture = _origFbo->getTexture();
    _texture2Uniform->texture = _bloomFbo->getTexture();

    _blurTextureUniform->texture = _bloomFbo->getTexture();

    distortCoord = glm::vec3 {0.0f, 0.0f, 0.00001f};

    SimpleVertexData meshData;

    std::vector<SimpleVertex> vertices =
            {
                glm::vec2{-1.0, -1.0},
                glm::vec2{-1.0,  1.0},
                glm::vec2{1.0, 1.0},
                glm::vec2{1.0, -1.0},
            };

    std::vector<std::uint32_t> indexes = {0, 1, 2, 0, 2, 3};

    meshData.vertices = std::move(vertices);
    meshData.indexes = std::move(indexes);

    _buffer = std::make_shared<GlVertexBuffer>(_engine, meshData);

    glViewport(0, 0, _engine.window().getWidth(), _engine.window().getHeight());
}

void GlRenderer::draw()
{
    auto needRender = [](uint32_t renderMask, uint32_t nodeMask)
    {
        return (renderMask == nodeMask) || (nodeMask & renderMask) != 0;
    };

    auto drawRenderCommand = [&](Command command)
    {
        auto glVertexBuffer = std::dynamic_pointer_cast<GlVertexBuffer>(
                command.vertexBuffer);
        auto glParticleBuffer = std::dynamic_pointer_cast<GlParticleBuffer>(
                command.vertexBuffer);

        if (glVertexBuffer || glParticleBuffer)
        {
            auto glProgram = std::dynamic_pointer_cast<GlProgram>(command.program);
            if (glProgram)
            {
                glProgram->activate();
                for (const auto &uniform: command.uniforms)
                {
                    uniform->activate();
                }

                if (command.scissor)
                {
                    glScissor((int) command.scissor->x,
                              (int) (_engine.window().getHeight() - command.scissor->w),
                              (int) (command.scissor->z - command.scissor->x),
                              (int) (command.scissor->w - command.scissor->y));
                }
                else
                {
                    glScissor(0, 0, _engine.window().getWidth(),
                              _engine.window().getHeight());
                }

                if (glVertexBuffer)
                {
                    if (command.sub)
                    {
                        glVertexBuffer->draw(command.sub->num, command.sub->offset);
                    }
                    else
                    {
                        glVertexBuffer->draw();
                    }
                }
                if (glParticleBuffer)
                {
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                    glParticleBuffer->draw();
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                }
            }
        }
    };

    _origFbo->drawToFrameBuffer([&]
    {
      for (const auto &command: _commands)
      {
          drawRenderCommand(command);
      }
    }
    );

    _bloomFbo->drawToFrameBuffer([&]
    {
      for (const auto &command: _commands)
      {
          if (needRender(0x1, command.mask))
          {
              drawRenderCommand(command);
          }
      }
    }
    );

    _commands.clear();

    _blurTextureUniform->texture = _bloomFbo->getTexture();

    _blurRadUniform->value = glm::vec2(0.0f / (float) _engine.window().getWidth(), 0.0);

    _blurFbo->drawToFrameBuffer([&]
    {
        _blurProgram->activate();
        _blurRadUniform->activate();
        _blurTextureUniform->activate();
        _buffer->draw();
    });

    _blurTextureUniform->texture = _blurFbo->getTexture();
    _blurRadUniform->value = glm::vec2(0.0, 0.0f / (float) _engine.window().getHeight());

    _bloomFbo->drawToFrameBuffer([&]()
     {
         _blurProgram->activate();
         _blurRadUniform->activate();
         _blurTextureUniform->activate();
         _buffer->draw();
     });

    glDisable(GL_SCISSOR_TEST);
    glDisable(GL_BLEND);

    glClearColor(0.0f, 0.3f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    _distortUniform->value = distortCoord;
    _timeUniform->value += _engine.scheduleManager().getCurrentDelta().count();

    _ppProgram->activate();
    _distortUniform->activate();
    _timeUniform->activate();
    _textureUniform->activateWithSlot(0);
    _texture2Uniform->activateWithSlot(1);
    _buffer->draw();
}

std::shared_ptr<VertexBuffer> GlRenderer::createVertexBuffer(const VertexData& data) const
{
    return _engine.createShared<GlVertexBuffer>(data);
}

std::shared_ptr<ShaderProgram> GlRenderer::createProgram(std::string_view name) const
{
    std::string name_string(name);
    if (_programs.count(name_string) > 0)
    {
        return _programs[name_string];
    }
    std::shared_ptr<ShaderProgram> program = nullptr;
    if (name == "draw")
    {
        program = _engine.createShared<GlSpriteProgram>();
    }
    else if (name == "particles")
    {
        program = _engine.createShared<GlParticleProgram>();
    }

    _programs[name_string] = program;
    return program;
}

std::shared_ptr<Texture> GlRenderer::createTexture(Bitmap bitmap) const
{
    return _engine.createShared<GlTexture>(std::move(bitmap));
}

std::shared_ptr<ParticleBuffer> GlRenderer::createParticleBuffer(std::vector<ParticleBuffer::ParticleData> data) const
{
    return _engine.createShared<GlParticleBuffer>(std::move(data));
}

std::shared_ptr<ParticleBuffer> GlRenderer::createParticleBuffer(size_t count) const
{
    return _engine.createShared<GlParticleBuffer>(count);
}

glm::vec2 GlRenderer::getRenderResolution() const
{
    GLint curViewport[4];
    glGetIntegerv(GL_VIEWPORT, curViewport);
    return glm::vec2(curViewport[2], curViewport[3]);
}

void GlRenderer::setViewport(glm::vec2 origin, glm::vec2 size) const
{
    glViewport(origin.x, origin.y, size.x, size.y);
}




