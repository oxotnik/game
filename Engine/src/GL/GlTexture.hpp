//
// Created by Андрей Марцинкевич on 29.11.21.
//

#ifndef GAME_GLTEXTURE_HPP
#define GAME_GLTEXTURE_HPP

#include <Bitmap.hpp>
#include <Texture.hpp>

class GlTexture : public Texture
{
public:
    explicit GlTexture(Bitmap bitmap);
    void active();

    uint32_t getId() const;

private:
    uint32_t _id;
};


#endif //GAME_GLTEXTURE_HPP
