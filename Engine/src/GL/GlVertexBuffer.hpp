//
// Created by Андрей Марцинкевич on 23.11.21.
//

#ifndef GAME_GLVERTEXBUFFER_HPP
#define GAME_GLVERTEXBUFFER_HPP

#include <VertexBuffer.hpp>
#include <VertexData.hpp>

class Engine;
class GlVertexBuffer final : public VertexBuffer
{
public:
    explicit GlVertexBuffer(const Engine& engine,
                    const VertexData& data);

    virtual ~GlVertexBuffer();

    void draw(size_t num, size_t offset);
    void draw();

private:
    uint32_t _VAO = -1;
    uint32_t _VBO = -1;
    uint32_t _IBO = -1;

    uint32_t _count;

    const Engine& _engine;
};


#endif //GAME_GLVERTEXBUFFER_HPP
