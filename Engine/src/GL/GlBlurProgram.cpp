//
// Created by Андрей Марцинкевич on 5.01.22.
//

#include "GlBlurProgram.hpp"

namespace
{
    std::string vs_program =
            R"(
VS_IN vec2 position;

VS_OUT vec2 oTexCoord;

void main()
{
    oTexCoord = position * 0.5 + vec2(0.5);
    gl_Position = vec4(position.x, position.y, 1.0, 1.0);
}
)";
    auto ps_program =
            R"(
uniform sampler2D uTexture;
uniform vec2 uDir;

PS_IN vec2 oTexCoord;

void main()
{
     PS_OUT = vec4(0.0);
     vec2 off1 = vec2(1.411764705882353) * uDir;
     vec2 off2 = vec2(3.2941176470588234) * uDir;
     vec2 off3 = vec2(5.176470588235294) * uDir;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord) * 0.1964825501511404;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord + off1) * 0.2969069646728344;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord - off1) * 0.2969069646728344;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord + off2) * 0.09447039785044732;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord - off2) * 0.09447039785044732;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord + off3) * 0.010381362401148057;
     PS_OUT += TEXTURE2D(uTexture, oTexCoord - off3) * 0.010381362401148057;
}
)";
}

GlBlurProgram::GlBlurProgram()
        : GlProgram({"position"}, vs_program, ps_program)
{

}
