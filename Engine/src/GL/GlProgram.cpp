//
// Created by Андрей Марцинкевич on 23.11.21.
//

#include "GlHeaders.hpp"
#include "GlProgram.hpp"
#include "GlTexture.hpp"

#include <glm/gtc/type_ptr.hpp>

#include <SDL.h>


GlProgram::GlProgram(std::initializer_list<const char*> attributes, std::string vs, std::string ps)
{
    std::string vs_header =
#if GLES20
    R"(
#version 100
#define VS_IN attribute
#define VS_OUT varying
    )";
#elif GL33
    R"(
#version 330 core
#define VS_IN in
#define VS_OUT out
    )";
#endif

    vs = vs_header + vs;

    const char* vs_cstr = vs.c_str();

    _vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(_vertexShader, 1, &vs_cstr, nullptr);
    glCompileShader(_vertexShader);

    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(_vertexShader, GL_COMPILE_STATUS, &success);

    if (!success)
    {
        glGetShaderInfoLog(_vertexShader, 512, nullptr, infoLog);

        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "ERROR::SHADER::VERTEX::COMPILATION_FAILED:\n %s \n %s \n", infoLog, vs.c_str());
    }

        std::string ps_header =
#if GLES20
    R"(
#version 100
precision mediump float;
#define PS_IN varying
#define PS_OUT gl_FragColor
#define TEXTURE2D texture2D
    )";
#elif GL33
    R"(
#version 330 core

#define PS_IN in
#define TEXTURE2D texture

out vec4 PS_OUT;
    )";
#endif

    ps = ps_header + ps;

    const char* ps_cstr = ps.c_str();

    _fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(_fragmentShader, 1, &ps_cstr, nullptr);
    glCompileShader(_fragmentShader);

    glGetShaderiv(_fragmentShader, GL_COMPILE_STATUS, &success);

    if (!success)
    {
        glGetShaderInfoLog(_fragmentShader, 512, nullptr, infoLog);
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED:\n %s \n %s \n", infoLog, ps.c_str());

    }

    _program = glCreateProgram();

    size_t index = 0;
    for (auto attribute : attributes)
    {
        glBindAttribLocation(_program, index++, attribute);
    }

    glAttachShader(_program, _vertexShader);
    glAttachShader(_program, _fragmentShader);
    glLinkProgram(_program);

    glGetProgramiv(_program, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(_program, 512, nullptr, infoLog);
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "ERROR::SHADER::LINK_FAILED:\n %s \n", infoLog);

    }
}

GlProgram::~GlProgram()
{
    glDeleteProgram(_program);
    glDeleteShader(_vertexShader);
    glDeleteShader(_fragmentShader);
}


void GlProgram::activate()
{
    glUseProgram(_program);
}

std::shared_ptr<TextureUniform> GlProgram::createTextureUniform(std::string_view name)
{
    return std::make_shared<GlTextureUniform>(std::static_pointer_cast<GlProgram>(shared_from_this()), name);
}

std::shared_ptr<Mat3Uniform> GlProgram::createMat3Uniform(std::string_view name)
{
    return std::make_shared<GlMat3Uniform>(std::static_pointer_cast<GlProgram>(shared_from_this()), name);
}

std::shared_ptr<Vec2Uniform> GlProgram::createVec2Uniform(std::string_view name)
{
    return std::make_shared<GlVec2Uniform>(std::static_pointer_cast<GlProgram>(shared_from_this()), name);
}

std::shared_ptr<Vec3Uniform> GlProgram::createVec3Uniform(std::string_view name)
{
    return std::make_shared<GlVec3Uniform>(std::static_pointer_cast<GlProgram>(shared_from_this()), name);
}

std::shared_ptr<FloatUniform> GlProgram::createFloatUniform(std::string_view name)
{
    return std::make_shared<GlFloatUniform>(std::static_pointer_cast<GlProgram>(shared_from_this()), name);
}

void GlTextureUniform::activateWithSlot(size_t slot)
{
    Uniform::activate();
    auto glTexture = std::dynamic_pointer_cast<GlTexture>(texture);
    if (glTexture)
    {
        //TODO: diff texture slots
        glActiveTexture(GL_TEXTURE0 + slot);
        glTexture->active();

        glUniform1i(_location, slot);
    }
}

void GlTextureUniform::activate()
{
    Uniform::activate();
    auto glTexture = std::dynamic_pointer_cast<GlTexture>(texture);
    if (glTexture)
    {
        //TODO: diff texture slots
        glActiveTexture(GL_TEXTURE0);
        glTexture->active();

        glUniform1i(_location, 0);
    }
}

GlTextureUniform::GlTextureUniform(const std::shared_ptr<GlProgram>& program, std::string_view name)
{
    _location = glGetUniformLocation(program->getProgramId(), name.data());
}

GlMat3Uniform::GlMat3Uniform(const std::shared_ptr<GlProgram> &program, std::string_view name)
{
    _location = glGetUniformLocation(program->getProgramId(), name.data());
}

void GlMat3Uniform::activate()
{
    Uniform::activate();
    glUniformMatrix3fv(_location, 1, GL_FALSE, glm::value_ptr(value));
}

GlVec2Uniform::GlVec2Uniform(const std::shared_ptr<GlProgram> &program, std::string_view name)
{
    _location = glGetUniformLocation(program->getProgramId(), name.data());
}

void GlVec2Uniform::activate()
{
    Uniform::activate();
    glUniform2f(_location, value.x, value.y);
}

GlVec3Uniform::GlVec3Uniform(const std::shared_ptr<GlProgram> &program, std::string_view name)
{
    _location = glGetUniformLocation(program->getProgramId(), name.data());
}

void GlVec3Uniform::activate()
{
    Uniform::activate();
    glUniform3f(_location, value.x, value.y, value.z);
}

GlFloatUniform::GlFloatUniform(const std::shared_ptr<GlProgram> &program, std::string_view name)
{
    _location = glGetUniformLocation(program->getProgramId(), name.data());
}

void GlFloatUniform::activate()
{
    Uniform::activate();
    glUniform1f(_location, value);
}
