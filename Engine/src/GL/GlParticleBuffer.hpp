//
// Created by Андрей Марцинкевич on 22.12.21.
//

#ifndef GAME_GLPARTICLEBUFFER_HPP
#define GAME_GLPARTICLEBUFFER_HPP

#include <ParticleBuffer.hpp>
#include <vector>

class GlParticleBuffer : public ParticleBuffer
{
public:
    explicit GlParticleBuffer(std::vector<ParticleData> data);

    explicit GlParticleBuffer(size_t capacity);
    void addData(ParticleData data) override;

    virtual ~GlParticleBuffer();

    void draw();

private:
    uint32_t _VAO = 0;
    uint32_t _VBO = 0;

    uint32_t _count;

    bool _isDynamic = false;
    uint32_t _capacity = 0;
    uint32_t _curPos = 0;

    void createBuffers();
    void createLayouts();


};


#endif //GAME_GLPARTICLEBUFFER_HPP
