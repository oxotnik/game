//
// Created by Андрей Марцинкевич on 23.11.21.
//

#include "GlVertexBuffer.hpp"

#include <GL/GlHeaders.hpp>
#include <Engine.hpp>
#include <Renderer.hpp>

GlVertexBuffer::GlVertexBuffer(const Engine &engine,
                               const VertexData& vertexData)
    : _engine(engine)
{

#if GLES20
    glGenVertexArraysOES(1, &_VAO);

    glBindVertexArrayOES(_VAO);
#elif GL33
    glGenVertexArrays(1, &_VAO);

    glBindVertexArray(_VAO);
#endif

    glGenBuffers(1, &_VBO);

    glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glBufferData(GL_ARRAY_BUFFER, vertexData.dataSize(), vertexData.data(), GL_STATIC_DRAW);

    for (size_t i = 0; i < vertexData.componentsNum(); ++i)
    {
        const auto& layout = vertexData.layoutFor(i);

        auto component_count = [&]()
        {
            switch(layout.type)
            {
                case VertexLayout::Type::Float: return 1;
                case VertexLayout::Type::Vec2: return 2;
                case VertexLayout::Type::Vec3: return 3;
                case VertexLayout::Type::Vec4:
                case VertexLayout::Type::Color: return 4;
            }
        }();

        auto component_type = [&]()
        {
            switch(layout.type)
            {
                case VertexLayout::Type::Float:
                case VertexLayout::Type::Vec2:
                case VertexLayout::Type::Vec3:
                case VertexLayout::Type::Vec4: return GL_FLOAT;
                case VertexLayout::Type::Color: return GL_UNSIGNED_BYTE;
            }
        }();

        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i, component_count,
                              component_type, layout.normalized, vertexData.vertexSize(),
                              (const GLvoid *)vertexData.offsetFor(i));
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    if (vertexData.indexes.get_size() != 0)
    {
        glGenBuffers(1, &_IBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _IBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     vertexData.indexes.get_size() * sizeof(std::uint32_t),
                     vertexData.indexes.get_data(), GL_STATIC_DRAW);

        _count = vertexData.indexes.get_size();
    }
    else
    {
        _count = vertexData.dataSize() / vertexData.vertexSize();
    }

}


void GlVertexBuffer::draw(size_t num, size_t offset)
{
#if GLES20
    glBindVertexArrayOES(_VAO);
#elif GL33
    glBindVertexArray(_VAO);
#endif

    if (_IBO != -1)
    {
        glDrawElements(GL_TRIANGLES,
                       static_cast<GLsizei>(num),
                       GL_UNSIGNED_INT,
                       reinterpret_cast<GLvoid *>(offset));
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, offset, num);
    }
}

GlVertexBuffer::~GlVertexBuffer()
{
    glDeleteBuffers(1, &_VBO);

    if (_IBO != -1)
    {
        glDeleteBuffers(1, &_IBO);
    }

#if GLES20
    glDeleteVertexArraysOES(1, &_VAO);
#elif GL33
    glDeleteVertexArrays(1, &_VAO);
#endif


    GlCheckError(__FILE__, __LINE__);
}

void GlVertexBuffer::draw()
{
    this->draw(_count, 0);
}

