//
// Created by Андрей Марцинкевич on 29.11.21.
//

#include "GlTexture.hpp"

#include "GlHeaders.hpp"

namespace
{
    GLenum getGlInternalFormat(Bitmap::Format bitmapFormat)
    {
        if (bitmapFormat == Bitmap::Format::R)
        {
#if GLES20
            return GL_R8_EXT;
#elif GL33
            return GL_R8;
#endif
        }

        if (bitmapFormat == Bitmap::Format::Rgba32F || bitmapFormat == Bitmap::Format::Rgb32F)
        {
#if GLES20
            return GL_RGBA32F_EXT;
#elif GL33
            return GL_RGBA32F;
#endif
        }
        else if (bitmapFormat == Bitmap::Format::Rgba16F)
        {
#if GLES20
            return GL_RGBA16F_EXT;
#elif GL33
            return GL_RGBA16F;
#endif
        }
        else if (bitmapFormat == Bitmap::Format::Rgb16F)
        {
#if GLES20
            return GL_RGB16F_EXT;
#elif GL33
            return GL_RGB16F;
#endif
        }
        else if (bitmapFormat == Bitmap::Format::Rgb)
        {
            return GL_RGB;
        }
        else
        {
            return GL_RGBA;
        }
    }

    GLenum getGlFormat(Bitmap::Format bitmapFormat)
    {
        switch (bitmapFormat)
        {
            case Bitmap::Format::R:
#if GLES20
                return GL_RED_EXT;
#elif GL33
                return GL_RED;
#endif

            case Bitmap::Format::Rgb:
            case Bitmap::Format::Rgb16F:
            case Bitmap::Format::Rgb32F:
                return GL_RGB;

            case Bitmap::Format::Rgba:
            case Bitmap::Format::Rgba16F:
            case Bitmap::Format::Rgba32F:
                return GL_RGBA;

        }
    }

    GLenum getGlType(Bitmap::Format bitmapFormat)
    {
        switch (bitmapFormat)
        {
            case Bitmap::Format::Rgb32F:
            case Bitmap::Format::Rgba32F:
                return GL_FLOAT;

            case Bitmap::Format::Rgb16F:
            case Bitmap::Format::Rgba16F:
#if GLES20
                return GL_HALF_FLOAT_OES;
#elif GL33
                return GL_HALF_FLOAT;
#endif

            case Bitmap::Format::Rgb:
            case Bitmap::Format::Rgba:
            case Bitmap::Format::R:
                return GL_UNSIGNED_BYTE;

        }
    }
}

GlTexture::GlTexture(Bitmap bitmap)
    : Texture(bitmap.getSize())
{
    glGenTextures(1, &_id);
    glBindTexture(GL_TEXTURE_2D, _id);

    const auto glInternalFormat = getGlInternalFormat(bitmap.getFormat());
    const auto glType = getGlType(bitmap.getFormat());
    const auto glFormat = getGlFormat(bitmap.getFormat());

    glTexImage2D(GL_TEXTURE_2D, 0, glInternalFormat, bitmap.getSize().x, bitmap.getSize().y, 0,
                 glFormat, glType, bitmap.getImage().get_size() == 0 ? nullptr : bitmap.getImage().get_data());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    if (bitmap.getImage().get_size() != 0 )
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }

    if (bitmap.getImage().get_size() != 0 )
    {
        glGenerateMipmap(GL_TEXTURE_2D);
    }
}

void GlTexture::active()
{
    glBindTexture(GL_TEXTURE_2D, _id);
}

uint32_t GlTexture::getId() const
{
    return _id;
}
