//
// Created by Андрей Марцинкевич on 23.11.21.
//

#ifndef GAME_GLSPRITEPROGRAM_HPP
#define GAME_GLSPRITEPROGRAM_HPP

#include <GL/GlProgram.hpp>
#include <GL/GlTexture.hpp>

#include <EventsManager.hpp>

#include <cstdint>

class Engine;

class GlSpriteProgram final : public GlProgram
{
public:
    explicit GlSpriteProgram();

private:
};


#endif //GAME_GLSPRITEPROGRAM_HPP
