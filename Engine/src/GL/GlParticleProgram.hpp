//
// Created by Андрей Марцинкевич on 22.12.21.
//

#ifndef GAME_GLPARTICLEPROGRAM_HPP
#define GAME_GLPARTICLEPROGRAM_HPP


#include "GlProgram.hpp"

class GlParticleProgram final : public GlProgram
{
public:
    explicit GlParticleProgram();
};


#endif //GAME_GLPARTICLEPROGRAM_HPP
