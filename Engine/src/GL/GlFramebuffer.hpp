//
// Created by Андрей Марцинкевич on 3.01.22.
//

#ifndef GAME_GLFRAMEBUFFER_HPP
#define GAME_GLFRAMEBUFFER_HPP


#include <Framebuffer.hpp>
#include <glm/glm.hpp>

class Engine;
class GlFramebuffer final : public Framebuffer
{
public:
    explicit GlFramebuffer(const Engine& engine, glm::vec2 size);
    void drawToFrameBuffer(std::function<void(void)> callback) override;

    virtual ~GlFramebuffer();

private:
    uint32_t _fbo;
};


#endif //GAME_GLFRAMEBUFFER_HPP
