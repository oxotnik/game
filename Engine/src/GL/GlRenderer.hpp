//
// Created by Андрей Марцинкевич on 23.11.21.
//

#ifndef GAME_GLRENDERER_HPP
#define GAME_GLRENDERER_HPP

#include <Renderer.hpp>
#include <unordered_map>
#include <string>

class SDL_Window;
class Engine;
class Framebuffer;
class TextureUniform;
class Vec2Uniform;
class Vec3Uniform;
class FloatUniform;
class GlVertexBuffer;
class GlRenderer final : public Renderer
{
public:
    explicit GlRenderer(const Engine& engine, SDL_Window* window);

    void init() override;

    void draw() override;

    std::shared_ptr<VertexBuffer> createVertexBuffer(const VertexData& data) const override;
    std::shared_ptr<ShaderProgram> createProgram(std::string_view name) const override;

    std::shared_ptr<ParticleBuffer> createParticleBuffer(std::vector<ParticleBuffer::ParticleData> data) const override;
    std::shared_ptr<ParticleBuffer> createParticleBuffer(size_t count) const override;

    std::shared_ptr<Texture> createTexture(Bitmap bitmap) const override;

    glm::vec2 getRenderResolution() const override;

    void setViewport(glm::vec2 origin, glm::vec2 size) const override;

private:
    const Engine& _engine;
    std::unique_ptr<void, void(*)(void*)> _drawContext;
    mutable std::unordered_map<std::string, std::shared_ptr<ShaderProgram>> _programs;

    std::shared_ptr<ShaderProgram> _ppProgram;
    std::shared_ptr<ShaderProgram> _blurProgram;

    std::shared_ptr<Framebuffer> _origFbo;
    std::shared_ptr<Framebuffer> _bloomFbo;
    std::shared_ptr<Framebuffer> _blurFbo;

    std::shared_ptr<TextureUniform>  _textureUniform;
    std::shared_ptr<TextureUniform>  _texture2Uniform;
    std::shared_ptr<Vec3Uniform>  _distortUniform;
    std::shared_ptr<FloatUniform>  _timeUniform;

    std::shared_ptr<GlVertexBuffer>  _buffer;

    std::shared_ptr<TextureUniform>  _blurTextureUniform;
    std::shared_ptr<Vec2Uniform>  _blurRadUniform;

};


#endif //GAME_GLRENDERER_HPP
