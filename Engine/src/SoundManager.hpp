//
// Created by Андрей Марцинкевич on 11.12.21.
//

#ifndef GAME_SOUNDMANAGER_HPP
#define GAME_SOUNDMANAGER_HPP

#include <memory>
#include <vector>
#include <string_view>
#include <mutex>

class Sound;

struct SoundParams
{
    size_t channels;
    size_t freq;
    enum Format
    {
        U8,
        S8,
        U16LSB,
        S16LSB,
        U16MSB,
        S16MSB,
        Unknown
    } format;
};
class Engine;
class SoundManager final
{
public:
    explicit SoundManager(const Engine& engine);
    ~SoundManager();

    std::shared_ptr<Sound> createSound(std::string_view filename, bool isLoop) const;

    SoundParams getParams() const;

private:
    mutable std::mutex audio_mutex;
    mutable std::vector<std::weak_ptr<Sound>> _sounds;
    uint32_t _deviceId;
    SoundParams _params;
    const Engine& _engine;

};


#endif //GAME_SOUNDMANAGER_HPP
