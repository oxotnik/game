//
// Created by Андрей Марцинкевич on 29.11.21.
//

#include "Bitmap.hpp"
#include "Engine.hpp"
#include "FileManager.hpp"

#include <string>
#include <vector>

#include <SDL.h>
#include <stbi/stb_image.h>

#include <GL/GlHeaders.hpp>

Bitmap::Bitmap(const Engine& engine, std::string_view filepath)
{
    SDL_RWops *file = SDL_RWFromFile(engine.fileManager().resourceLocation(std::string{filepath}).c_str(), "rb");

    if (file == nullptr)
    {
        throw std::runtime_error("Failed to open file: " + std::string{filepath});
    }

    Sint64 length = SDL_RWsize(file);
    std::vector<unsigned char> data(length);

    SDL_RWread(file, data.data(), length, 1);
    SDL_RWclose(file);


    int width, height, ch;
    stbi_set_flip_vertically_on_load(false);
    auto img = stbi_load_from_memory(data.data(), data.size(), &width, &height, &ch, 0);
    if (img == nullptr)
    {
        throw std::runtime_error("unsupported file format: " + std::string{filepath});
    }

    auto getFmt = [](int ch)
    {
        switch (ch)
        {
            case 1:
                return Format::R;
            case 3:
                return Format::Rgb;
            case 4:
                return Format::Rgba;

            default:
                throw std::invalid_argument("Incorect chanels size from TGA");
        }
    };

    _format = getFmt(ch);
    size_t imgSize = width*height*ch;
    _image = {img, imgSize, [img]()
    {
        stbi_image_free(img);
    }};

    _size = glm::vec2(width, height);
}

const ext::array_view<unsigned char>& Bitmap::getImage() const
{
    return _image;
}

Bitmap::Format Bitmap::getFormat() const
{
    return _format;
}

glm::vec2 Bitmap::getSize() const
{
    return _size;
}

Bitmap::Bitmap(Bitmap::Format format, ext::array_view<unsigned char> image, glm::vec2 size)
        : _format(format), _image(std::move(image)), _size(size)
{}
