//
// Created by Андрей Марцинкевич on 1.12.21.
//

#ifndef GAME_SPRITE_HPP
#define GAME_SPRITE_HPP

#include <Node.hpp>
#include <string_view>
#include <Renderer.hpp>

class Engine;
class Texture;
class VertexBuffer;
class ShaderProgram;
class TextureUniform;
class Mat3Uniform;
class Vec2Uniform;

#include <VertexDataHelper.hpp>

struct SpriteVertex
{
    glm::vec2 position{};
    glm::vec2 texcoord{};
    ext::color color = glm::vec4(1.0f, 1.0f,1.0f,1.0f);
};

using SpriteVertexData =
    VertexDataCreator<SpriteVertex,
        LayoutGen<glm::vec2, glm::vec2, ext::color>,
        OffsetsGen<offsetof(SpriteVertex, position)
                 , offsetof(SpriteVertex, texcoord)
                 , offsetof(SpriteVertex, color)
                  >
        >;

class Sprite : public Node
{
public:
    explicit Sprite(const Engine& engine, std::string_view filepath);

protected:
    void visitSelf() override;

private:
    Renderer::Command _command;

    std::shared_ptr<Vec2Uniform> _screenSizeUniform;
    std::shared_ptr<Mat3Uniform>  _transformUniform;
    std::shared_ptr<TextureUniform>  _textureUniform;
};


#endif //GAME_SPRITE_HPP
