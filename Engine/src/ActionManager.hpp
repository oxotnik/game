//
// Created by Андрей Марцинкевич on 10.01.22.
//

#ifndef GAME_ACTIONMANAGER_HPP
#define GAME_ACTIONMANAGER_HPP

#include <memory>
#include <Utils/ProtectedVector.hpp>
#include "ScheduleManager.hpp"
#include "glm/glm.hpp"

class Node;
class Action
{
public:
    explicit Action(const fseconds &duration);
    virtual void startWithTarget(Node* target);

    size_t getTag() const;
    void setTag(size_t tag);

    void step(fseconds dt);
    virtual void update(float value){};

    [[nodiscard]] Node* getTarget() const;

    bool isDone();

protected:
    fseconds _duration;
    fseconds _elapsed = 0s;
    size_t _tag{0};

    Node* _target;
};

class MoveBy : public Action
{
public:
    MoveBy(glm::vec2 offset, fseconds duration);

    void startWithTarget(Node *target) override;

    void update(float value) override;

private:
    glm::vec2 _offset;
    glm::vec2 _startPosiion;
};

class ActionManager
{
public:
    void addAction(std::shared_ptr<Action> action) const;
    void removeAction(const std::shared_ptr<Action>& action) const;
    void removeActionByTag(size_t tag) const;
    void removeActionForNode(Node* node) const;

    void update(fseconds dt) const;

private:
   mutable ProtectedVector<std::shared_ptr<Action>> _actions;
};


#endif //GAME_ACTIONMANAGER_HPP
