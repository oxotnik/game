//
// Created by Андрей Марцинкевич on 22.12.21.
//

#include "ParticleEmitter.hpp"
#include "ParticleBuffer.hpp"
#include <Engine.hpp>
#include <ShaderProgram.hpp>
#include <Window.hpp>

#include <glm/gtx/vector_angle.hpp>

#include <random>

ParticleEmitter::ParticleEmitter(const Engine &engine)
        : Node(engine)
{
    _command.program = engine.renderer().createProgram("particles");

    _screenSizeUniform = _command.program->createVec2Uniform("uScreenSize");
    _resolutionUniform = _command.program->createVec2Uniform("uResolutionSize");
    _timeUniform = _command.program->createFloatUniform("uTime");
    _cameraUniform = _command.program->createMat3Uniform("uCameraMatrix");

    _command.uniforms.push_back(_screenSizeUniform);
    _command.uniforms.push_back(_timeUniform);
    _command.uniforms.push_back(_cameraUniform);
    _command.uniforms.push_back(_resolutionUniform);

    _resolutionUniform->onActivate = [=]()
    {
        _resolutionUniform->value = _engine.renderer().getRenderResolution();
    };

    this->scheduleUpdate();
}

void ParticleEmitter::update(fseconds dt)
{
    _timeUniform->value += dt.count();

    if (_dynamicMode)
    {
        _lastGenTime += dt.count();
        _totalTime += dt.count();

        if (_lastGenTime >= _freq && _totalTime < _lifeTime)
        {
            auto genCount = std::min(_count, _lastGenTime / _freq);
            _lastGenTime = 0;
            auto startPos = (getTransform() * glm::vec3(1.0)).xy();

            for (size_t i = 0; i < genCount; ++i)
            {
                static std::mt19937 gen{};
                static std::normal_distribution<float> angleGen(0, 360);
                static std::normal_distribution<float> velGen(1, 500);

                ParticleBuffer::ParticleData particle;
                particle.pos = startPos;

                auto vector = glm::rotate(glm::vec2{1.0f, 0.0f}, glm::radians(angleGen(gen)));
                auto speed = velGen(gen);

                particle.velocity = vector * speed;
                particle.color = glm::vec4(1.0, 1.0, 1.0, 1.0);
                particle.phase = _timeUniform->value;

                auto particleBuffer = std::static_pointer_cast<ParticleBuffer>(_command.vertexBuffer);
                particleBuffer->addData(particle);
            }
        }
    }
}

void ParticleEmitter::visitSelf()
{
    const auto& win = _engine.window();
    _screenSizeUniform->value = _engine.getVirtualResolution();
    _cameraUniform->value = glm::inverse(_engine.getCamera()->getTransform());

    _command.mask = _renderMask;

    _engine.renderer().addCommand(_command);
}

void ParticleEmitter::start(size_t count)
{
    _timeUniform->value = 0;

    std::vector<ParticleBuffer::ParticleData> particles;
    particles.reserve(count);

    auto startPos = (getTransform() * glm::vec3(1.0)).xy();

    static std::mt19937 gen{};
    static std::normal_distribution<float> angleGen(0, 360);
    static std::normal_distribution<float> velGen(1, 500);

    for (size_t i = 0; i < count; ++i)
    {
        ParticleBuffer::ParticleData particle;
        particle.pos = startPos;

        auto vector = glm::rotate(glm::vec2{1.0f, 0.0f}, glm::radians(angleGen(gen)));
        auto speed = velGen(gen);

        particle.velocity = vector * speed;
        particle.color = glm::vec4(1.0, 1.0, 0.0, 1.0);

        particles.push_back(particle);
    }

    _command.vertexBuffer = _engine.renderer().createParticleBuffer(std::move(particles));
}

void ParticleEmitter::start(size_t count, float freq, float lifeTime)
{
    _count = count;
    _command.vertexBuffer = _engine.renderer().createParticleBuffer(count);
    _dynamicMode = true;
     _freq = freq;
     _lastGenTime = 0;
     _totalTime = 0;
     _lifeTime = lifeTime;
    _timeUniform->value = 0;
}
