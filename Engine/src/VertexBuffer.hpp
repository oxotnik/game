//
// Created by Андрей Марцинкевич on 1.12.21.
//

#ifndef GAME_VERTEXBUFFER_HPP
#define GAME_VERTEXBUFFER_HPP


class VertexBuffer
{
public:
    virtual ~VertexBuffer() = default;
};


#endif //GAME_VERTEXBUFFER_HPP
