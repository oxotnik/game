//
// Created by Андрей Марцинкевич on 11.12.21.
//

#include "ImguiManager.hpp"

#include <Texture.hpp>
#include <Engine.hpp>
#include <Renderer.hpp>
#include <Window.hpp>
#include <ShaderProgram.hpp>
#include <ScheduleManager.hpp>
#include <Sprite.hpp>

#include <imgui.h>

#include <Utils/KeyCodes.hpp>



ImguiManager::ImguiManager(const Engine &engine)
        : _engine(engine)
        , _eventHandler(engine.eventsManager(), ext::genUniqueObjectId())
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();
    ImGuiIO &io = ImGui::GetIO();
    io.BackendPlatformName = "custom_micro_engine";

    io.KeyMap[ImGuiKey_Tab] = static_cast<size_t>(KeyCode::TAB);
    io.KeyMap[ImGuiKey_LeftArrow] = static_cast<size_t>(KeyCode::LEFT);
    io.KeyMap[ImGuiKey_RightArrow] = static_cast<size_t>(KeyCode::RIGHT);
    io.KeyMap[ImGuiKey_UpArrow] = static_cast<size_t>(KeyCode::UP);
    io.KeyMap[ImGuiKey_DownArrow] = static_cast<size_t>(KeyCode::DOWN);
    io.KeyMap[ImGuiKey_PageUp] = static_cast<size_t>(KeyCode::PAGEUP);
    io.KeyMap[ImGuiKey_PageDown] = static_cast<size_t>(KeyCode::PAGEDOWN);
    io.KeyMap[ImGuiKey_Home] = static_cast<size_t>(KeyCode::HOME);
    io.KeyMap[ImGuiKey_End] = static_cast<size_t>(KeyCode::END);
    io.KeyMap[ImGuiKey_Insert] = static_cast<size_t>(KeyCode::INSERT);
    io.KeyMap[ImGuiKey_Delete] = static_cast<size_t>(KeyCode::DELETE);
    io.KeyMap[ImGuiKey_Backspace] = static_cast<size_t>(KeyCode::BACKSPACE);
    io.KeyMap[ImGuiKey_Space] = static_cast<size_t>(KeyCode::SPACE);
    io.KeyMap[ImGuiKey_Enter] = static_cast<size_t>(KeyCode::RETURN);
    io.KeyMap[ImGuiKey_Escape] = static_cast<size_t>(KeyCode::ESCAPE);
    io.KeyMap[ImGuiKey_A] = static_cast<size_t>(KeyCode::A);
    io.KeyMap[ImGuiKey_C] = static_cast<size_t>(KeyCode::C);
    io.KeyMap[ImGuiKey_V] = static_cast<size_t>(KeyCode::V);
    io.KeyMap[ImGuiKey_X] = static_cast<size_t>(KeyCode::X);
    io.KeyMap[ImGuiKey_Y] = static_cast<size_t>(KeyCode::Y);
    io.KeyMap[ImGuiKey_Z] = static_cast<size_t>(KeyCode::Z);

    io.RenderDrawListsFn = nullptr;

    int width = 0;
    int height = 0;
    unsigned char *dataPtr;
    io.Fonts->GetTexDataAsRGBA32(&dataPtr, &width, &height);
    ext::array_view<unsigned char> image(dataPtr, (width * height * 4));

    Bitmap bitmap(Bitmap::Format::Rgba, std::move(image), glm::vec2{width, height});

    //TODO: texture map
    //io.Fonts->TexID = (void *) 0x42;

    io.SetClipboardTextFn = [](void *userData, const char *text)
    {
        auto self = static_cast<ImguiManager*>(userData);
        self->_engine.window().setClipboardText(text);
    };

    io.GetClipboardTextFn = [](void *userData) -> const char *
    {
        auto self = static_cast<ImguiManager*>(userData);
        return self->_engine.window().getClipboardText().data();
    };

    io.ClipboardUserData = this;


    _command.program = engine.renderer().createProgram("draw");

    _textureUniform = _command.program->createTextureUniform("uTexture");
    _textureUniform->texture = engine.renderer().createTexture(std::move(bitmap));

    _screenSizeUniform = _command.program->createVec2Uniform("uScreenSize");
    _transformUniform = _command.program->createMat3Uniform("uTransform");

    _command.uniforms.push_back(_transformUniform);
    _command.uniforms.push_back(_screenSizeUniform);
    _command.uniforms.push_back(_textureUniform);

    _eventHandler += [](const TextInputEvent &e)
    {
        ImGuiIO &io = ImGui::GetIO();
        io.AddInputCharactersUTF8(e.string.data());
    };

    _eventHandler += [](const KeyEvent &e)
    {
        ImGuiIO &io = ImGui::GetIO();
        size_t key = static_cast<size_t>(e.key);

        io.KeysDown[key] = e.type == KeyEvent::Type::KeyDown;

//        //TODO: add mod state to event
//        uint32_t mod_keys_mask = SDL_GetModState();
//        io.KeyShift = ((mod_keys_mask & KMOD_SHIFT) != 0);
//        io.KeyCtrl = ((mod_keys_mask & KMOD_CTRL) != 0);
//        io.KeyAlt = ((mod_keys_mask & KMOD_ALT) != 0);
//        io.KeySuper = ((mod_keys_mask & KMOD_GUI) != 0);
    };

    _eventHandler += [this](const MouseEvent &e)
    {
        // TODO: сделать красиво и пиздато
        if (e.type == MouseEvent::Type::LButtonUp)
        {
            _lMousePressed = false;
        }
        else if (e.type == MouseEvent::Type::RButtonUp)
        {
            _rMousePressed = false;
        }
        else if (e.type == MouseEvent::Type::MButtonDown)
        {
            _mMousePressed = false;
        }
        else if (e.type == MouseEvent::Type::LButtonDown)
        {
            _lMousePressed = true;
        }
        else if (e.type == MouseEvent::Type::RButtonDown)
        {
            _rMousePressed = true;
        }
        else if (e.type == MouseEvent::Type::MButtonDown)
        {
            _mMousePressed = true;
        }
        else if (e.type == MouseEvent::Type::Move)
        {
            _mousePos = glm::vec2{e.x, e.y};
        }
    };

    _eventHandler += [this](const MouseWheelEvent &e)
    {
        if (e.value > 0)
        {
            _mouseWheel = 1;
        }
        if (e.value < 0)
        {
            _mouseWheel = -1;
        }
    };
}

void ImguiManager::visit()
{
    ImGuiIO &io = ImGui::GetIO();

    io.DisplaySize = ImVec2(float(_engine.window().getWidth()), float(_engine.window().getHeight()));

    io.MousePos = {_mousePos.x, _mousePos.y};

    io.MouseDown[0] = _lMousePressed;
    io.MouseDown[1] = _rMousePressed;
    io.MouseDown[2] = _mMousePressed;

    io.MouseWheel = _mouseWheel;
    _mouseWheel = 0.0f;

    io.DeltaTime = _engine.scheduleManager().getCurrentDelta().count();

    ImGui::NewFrame();

//    ImGui::ShowDemoWindow(&show_demo_window);

    for (auto& item : _uis.top())
    {
        item->visit();
    }

    ImGui::Render();

    auto drawData = ImGui::GetDrawData();

    static_assert(sizeof(SpriteVertex) == sizeof(ImDrawVert));
    static_assert(sizeof(SpriteVertex::position) == sizeof(ImDrawVert::pos));
    static_assert(sizeof(SpriteVertex::texcoord) == sizeof(ImDrawVert::uv));
    static_assert(sizeof(SpriteVertex::color) == sizeof(ImDrawVert::col));
    static_assert(offsetof(SpriteVertex, position) == offsetof(ImDrawVert, pos));
    static_assert(offsetof(SpriteVertex, texcoord) == offsetof(ImDrawVert, uv));
    static_assert(offsetof(SpriteVertex, color) == offsetof(ImDrawVert, col));

    for (int n = 0; n < drawData->CmdListsCount; ++n)
    {
        const ImDrawList *cmd_list = drawData->CmdLists[n];

        auto vertex_data = reinterpret_cast<SpriteVertex *>(cmd_list->VtxBuffer.Data);
        size_t vert_count = static_cast<size_t>(cmd_list->VtxBuffer.size());

        const uint32_t *indexes = cmd_list->IdxBuffer.Data;
        size_t index_count = static_cast<size_t>(cmd_list->IdxBuffer.size());

        SpriteVertexData meshData;
        meshData.vertices = {vertex_data, vert_count};
        meshData.indexes = {indexes, index_count};

        auto vertexBuffer = _engine.renderer().createVertexBuffer(std::move(meshData));

        _command.vertexBuffer = std::move(vertexBuffer);
        const auto &win = _engine.window();
        _screenSizeUniform->value.x = win.getWidth();
        _screenSizeUniform->value.y = win.getHeight();

        _transformUniform->value = glm::mat3(1.0);

        size_t offset = 0;
        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd *pcmd = &cmd_list->CmdBuffer[cmd_i];

            _command.sub.emplace();
            _command.sub->num = pcmd->ElemCount;
            _command.sub->offset = offset * sizeof(std::uint32_t);

            _command.scissor = glm::vec4(pcmd->ClipRect.x, pcmd->ClipRect.y, pcmd->ClipRect.z, pcmd->ClipRect.w);

            _engine.renderer().addCommand(_command);

            offset += pcmd->ElemCount;
        }
    }
}

//void ImguiManager::addMenuItem(std::shared_ptr<MenuItem> item)
//{
//    _menuItems.push_back(std::move(item));
//}
//
//void ImguiManager::removeMenuItem(const std::shared_ptr<MenuItem>& item)
//{
//    auto it = std::find(std::begin(_menuItems), std::end(_menuItems), item);
//    _menuItems.erase(it);
//}

void Button::visit()
{
    ImGui::Button(_text.c_str());
}

void BeginItem::visit()
{
    ImGui::Begin(_text.c_str());
}

void EndItem::visit()
{
    ImGui::End();
}
