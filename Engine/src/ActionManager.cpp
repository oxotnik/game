//
// Created by Андрей Марцинкевич on 10.01.22.
//

#include "ActionManager.hpp"
#include "Node.hpp"

size_t Action::getTag() const
{
    return _tag;
}

void Action::setTag(size_t tag)
{
    _tag = tag;
}

void Action::step(fseconds dt)
{
    _elapsed += dt;
    this->update(glm::clamp( _elapsed.count()/ _duration.count(), 0.0f, 1.0f));
}

Action::Action(const fseconds &duration)
        : _duration(duration)
{

}

bool Action::isDone()
{
    return _elapsed >= _duration;
}

void Action::startWithTarget(Node* target)
{
    _target = target;
}

Node *Action::getTarget() const
{
    return _target;
}

void ActionManager::addAction(std::shared_ptr<Action> action) const
{
    _actions.add(std::move(action));
}

void ActionManager::removeAction(const std::shared_ptr<Action>& action) const
{
    _actions.remove(action);
}

void ActionManager::removeActionByTag(size_t tag) const
{
    _actions.removeIf([tag](const auto& action){return action->getTag() == tag;});
}

void ActionManager::update(fseconds dt) const
{
    _actions.forEach([&](auto action)
     {
        action->step(dt);
        if (action->isDone())
        {
            _actions.remove(action);
        }
     });
}

void ActionManager::removeActionForNode(Node* node) const
{
    _actions.removeIf([node](const auto& action){return action->getTarget() == node;});
}

MoveBy::MoveBy(glm::vec2 offset, fseconds duration)
        : _offset(offset)
        , Action(duration)
{

}

void MoveBy::update(float value)
{
    Action::update(value);
    if (_target)
    {
        auto delta = glm::mix(glm::vec2(0), _offset, value);
        _target->setPosition(_startPosiion + delta);
    }
}

void MoveBy::startWithTarget(Node *target)
{
    Action::startWithTarget(target);
    _startPosiion = target->getPosition();
}
