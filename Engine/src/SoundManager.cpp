//
// Created by Андрей Марцинкевич on 11.12.21.
//

#include "SoundManager.hpp"
#include "FileManager.hpp"
#include "Sound.hpp"
#include "Engine.hpp"

#include <SDL.h>

SoundManager::SoundManager(const Engine& engine)
 : _engine(engine)
{
    SDL_AudioSpec audioSpec{};
    audioSpec.channels = 2;
    audioSpec.freq = 44100;
    audioSpec.samples = 1024;
    audioSpec.format = AUDIO_S16LSB;
    audioSpec.userdata = this;
    audioSpec.callback = [](void *userdata, uint8_t *stream, int len)
    {
        SoundManager &self = *static_cast<SoundManager *>(userdata);
        std::lock_guard guard(self.audio_mutex);
        SDL_memset(stream, 0, len);
        for (auto &weakSound: self._sounds)
        {
            if (!weakSound.expired())
            {
                auto sound = weakSound.lock();
                if (sound->isPlaying())
                {
                    auto &buffer = sound->_buffer;

                    auto amount = buffer.size - buffer.current_pos;
                    if (amount > len)
                    {
                        amount = len;
                    }

                    SDL_MixAudioFormat(stream,
                                       buffer.start + buffer.current_pos,
                                       AUDIO_S16LSB,
                                       amount,
                                       SDL_MIX_MAXVOLUME * sound->getVolume());

//                    buffer.current_pos = 0;
//
//                    SDL_MixAudioFormat(stream + amount,
//                                       buffer.start + buffer.current_pos,
//                                       AUDIO_S16LSB,
//                                       len - amount,
//                                       SDL_MIX_MAXVOLUME * sound->getVolume());

                    buffer.current_pos += amount;

                    if (buffer.current_pos >= buffer.size)
                    {
                        if (sound->isLoop())
                        {
                            buffer.current_pos = 0;
                        }
                        else
                        {
                            sound->stop();
                        }
                    }
                }
            }
        }

        self._sounds.erase(std::remove_if(self._sounds.begin(),
                                          self._sounds.end(),
                                          [](auto &sound)
                                          { return sound.expired(); }), self._sounds.end());
    };

    const int32_t allow_changes = 0;
    const char*   device_name       = nullptr;
    const int32_t is_capture_device = 0;

    SDL_AudioSpec returned{};
    _deviceId = SDL_OpenAudioDevice(
            device_name, is_capture_device, &audioSpec, &returned, allow_changes);
    if (_deviceId == 0)
    {
        throw std::runtime_error("Can't open Audio Device");
    }

    _params.freq = returned.freq;
    _params.channels = returned.channels;

    _params.format = [&]()
    {
        switch (returned.format)
        {
            case AUDIO_U8: return SoundParams::Format::U8;
            case AUDIO_S8: return SoundParams::Format::S8;
            case AUDIO_S16LSB: return SoundParams::Format::S16LSB;
            case AUDIO_U16LSB: return SoundParams::Format::U16LSB;
            case AUDIO_S16MSB: return SoundParams::Format::S16MSB;
            case AUDIO_U16MSB: return SoundParams::Format::U16MSB;
            default: return SoundParams::Format::Unknown;
        }
    }();

    SDL_PauseAudioDevice(_deviceId, SDL_FALSE);
}

std::shared_ptr<Sound> SoundManager::createSound(std::string_view filename, bool isLoop) const
{
    std::lock_guard guard(audio_mutex);

    auto sound = std::shared_ptr<Sound>(new Sound(_engine.fileManager().resourceLocation(std::string{filename}), isLoop, _params));
    _sounds.push_back(sound);
    return sound;
}

SoundParams SoundManager::getParams() const
{
    return _params;
}

SoundManager::~SoundManager()
{
    SDL_PauseAudioDevice(_deviceId, SDL_TRUE);
    SDL_CloseAudioDevice(_deviceId);
}
