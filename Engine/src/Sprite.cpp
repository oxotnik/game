//
// Created by Андрей Марцинкевич on 1.12.21.
//

#include "Sprite.hpp"
#include "Bitmap.hpp"

#include <Engine.hpp>
#include <Renderer.hpp>
#include <Window.hpp>
#include <ShaderProgram.hpp>


Sprite::Sprite(const Engine& engine, std::string_view filepath)
    : Node(engine)
{
    Bitmap bitmap(engine, filepath);

    _contentSize = bitmap.getSize();

    SpriteVertexData vertexData;

    std::vector<SpriteVertex> vertices;

    vertices.emplace_back();
    vertices.back().position = {0.0, 0.0};
    vertices.back().texcoord = {0.0, 0.0};

    vertices.emplace_back();
    vertices.back().position = {0.0, bitmap.getSize().y};
    vertices.back().texcoord = {0.0, 1.0};

    vertices.emplace_back();
    vertices.back().position = {bitmap.getSize().x, bitmap.getSize().y};
    vertices.back().texcoord = {1.0, 1.0};

    vertices.emplace_back();
    vertices.back().position = {bitmap.getSize().x, 0.0};
    vertices.back().texcoord = {1.0, 0.0};

    std::vector<std::uint32_t> indexes;

    indexes.emplace_back(0);
    indexes.emplace_back(2);
    indexes.emplace_back(3);

    indexes.emplace_back(0);
    indexes.emplace_back(1);
    indexes.emplace_back(2);

    vertexData.vertices = std::move(vertices);
    vertexData.indexes = std::move(indexes);


    _command.vertexBuffer = engine.renderer().createVertexBuffer(vertexData);
    _command.program = engine.renderer().createProgram("draw");

    _textureUniform = _command.program->createTextureUniform("uTexture");
    _textureUniform->texture = engine.renderer().createTexture(std::move(bitmap));

    _screenSizeUniform = _command.program->createVec2Uniform("uScreenSize");
    _transformUniform = _command.program->createMat3Uniform("uTransform");

    _command.uniforms.push_back(_transformUniform);
    _command.uniforms.push_back(_screenSizeUniform);
    _command.uniforms.push_back(_textureUniform);

}

void Sprite::visitSelf()
{
    //useShader
    //set Uniform
    //set Texture
    // draw
    const auto& win = _engine.window();
    _screenSizeUniform->value = _engine.getVirtualResolution();

    _transformUniform->value = glm::inverse(_engine.getCamera()->getTransform()) * this->getTransform();

    _command.mask = _renderMask;

    _engine.renderer().addCommand(_command);
}
