//
// Created by Андрей Марцинкевич on 18.11.21.
//

#include "SdlWindow.hpp"
#include <Engine.hpp>

#include <GL/GlHeaders.hpp>

#include <SDL/SdlRenderer.hpp>
#include <GL/GlRenderer.hpp>

#include <EventsManager.hpp>

SdlWindow::SdlWindow(const Engine& engine,
                     std::string_view name,
                     size_t width,
                     size_t height,
                     RenderMode renderMode)
 : Window(width, height)
 , _engine(engine)
 , _renderMode(renderMode)
{
    SDL_version version   = { 0, 0, 0 };

    SDL_GetVersion(&version);

    SDL_Log("SDL Version: %d.%d.%d", version.major, version.minor, version.patch);

    SDL_Init(SDL_INIT_EVERYTHING);

    Uint32 windowFlags = SDL_WINDOW_SHOWN;

    if (_renderMode == RenderMode::OpenGl)
    {
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

#if GLES20
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        windowFlags |= SDL_WINDOW_FULLSCREEN;
#elif GL33
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
#endif

        windowFlags |=  SDL_WINDOW_OPENGL;
    }

    _window = {SDL_CreateWindow(name.data(),
                             SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                             width,
                             height,
                             windowFlags | SDL_WINDOW_ALLOW_HIGHDPI), SDL_DestroyWindow};



    int w, h;
    SDL_GL_GetDrawableSize(_window.get(), &w, &h);

    _scaleFactor.x = w / (float)_width;
    _scaleFactor.y = h / (float)_height;

    _width = w;
    _height = h;
}

void SdlWindow::update()
{
    SDL_Event e;

    const auto& event_manager = _engine.eventsManager();

    while (SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case SDL_QUIT:
            {
                event_manager.invokeEvent(SystemEvent{SystemEvent::Type::Quit});

                break;
            }

            case SDL_MOUSEBUTTONDOWN :
            {
                auto button = [&]()
                {
                    switch (e.button.button)
                    {
                        case SDL_BUTTON_LEFT: return MouseEvent::Type::LButtonDown;
                        case SDL_BUTTON_RIGHT: return MouseEvent::Type::RButtonDown;
                        case SDL_BUTTON_MIDDLE: return MouseEvent::Type::MButtonDown;
                    }
                }();

                event_manager.invokeEvent(MouseEvent{button, int(e.button.x * _scaleFactor.x), int(e.button.y * _scaleFactor.y)});

                break;
            }

            case SDL_MOUSEBUTTONUP :
            {
                auto button = [&]()
                {
                    switch (e.button.button)
                    {
                        case SDL_BUTTON_LEFT: return MouseEvent::Type::LButtonUp;
                        case SDL_BUTTON_RIGHT: return MouseEvent::Type::RButtonUp;
                        case SDL_BUTTON_MIDDLE: return MouseEvent::Type::MButtonUp;
                    }
                }();
                event_manager.invokeEvent(MouseEvent{button, int(e.button.x * _scaleFactor.x), int(e.button.y * _scaleFactor.y)});

                break;
            }

            case SDL_MOUSEMOTION:
            {
                event_manager.invokeEvent(MouseEvent{MouseEvent::Type::Move, int(e.button.x * _scaleFactor.x), int(e.button.y * _scaleFactor.y)});

                break;
            }

            case SDL_KEYDOWN:
            {
                if (!e.key.repeat)
                {
                    event_manager.invokeEvent(KeyEvent{KeyEvent::Type::KeyDown, static_cast<KeyCode>(e.key.keysym.scancode)});
                }

                break;
            }

            case SDL_KEYUP:
            {
                if (!e.key.repeat)
                {
                    event_manager.invokeEvent(KeyEvent{KeyEvent::Type::KeyUp, static_cast<KeyCode>(e.key.keysym.scancode)});
                }

                break;
            }

            case SDL_TEXTINPUT:
            {
                event_manager.invokeEvent(TextInputEvent(e.text.text));
                break;
            }

            case SDL_WINDOWEVENT:
            {
                if (e.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
                {
                    event_manager.invokeEvent(SystemEvent{SystemEvent::Type::Pause});

                }
                else if (e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
                {
                    event_manager.invokeEvent(SystemEvent{SystemEvent::Type::Resume});

                }
                else if (e.window.event == SDL_WINDOWEVENT_RESIZED)
                {
                    event_manager.invokeEvent(ResizeEvent{e.window.data1, e.window.data2});
                }
                else if (e.window.event == SDL_WINDOWEVENT_CLOSE)
                {
                    event_manager.invokeEvent(SystemEvent{SystemEvent::Type::Quit});
                }

                break;
            }
        }
    }
}

std::unique_ptr<Renderer> SdlWindow::createRenderer()
{
    if (_renderMode == RenderMode::OpenGl)
    {
        return _engine.createUnique<GlRenderer>(_window.get());
    }
    else
    {
        return nullptr;
//        return _engine.createUnique<SdlRenderer>(_window);
    }
}

void SdlWindow::swap()
{
    if (_renderMode == RenderMode::OpenGl)
    {
        SDL_GL_SwapWindow(_window.get());
    }
}

std::string_view SdlWindow::getClipboardText() const
{
    _clipboard = SDL_GetClipboardText();
    return _clipboard;
}

void SdlWindow::setClipboardText(std::string_view text) const
{
    SDL_SetClipboardText(text.data());
}

void SdlWindow::resize(size_t width, size_t height)
{
    int w, h;
    int ww, hh;

    SDL_GL_GetDrawableSize(_window.get(), &w, &h);
    _scaleFactor.x = (int) h / (float) height;
    _scaleFactor.y = (int) w / (float) width;

    _width = w;
    _height = h;
}