//
// Created by Андрей Марцинкевич on 1.12.21.
//

#ifndef GAME_TEXTURE_HPP
#define GAME_TEXTURE_HPP


#include <glm/vec2.hpp>

class Texture
{
public:
    explicit Texture(glm::vec2 size);
    virtual ~Texture() = default;

    glm::vec2 getSize() const;

protected:
    glm::vec2 _size;
};


#endif //GAME_TEXTURE_HPP
