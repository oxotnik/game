//
// Created by Андрей Марцинкевич on 8.12.21.
//

#ifndef GAME_SOUND_HPP
#define GAME_SOUND_HPP

#include <string_view>

class SoundManager;
class SoundParams;
class Sound
{
public:
    friend SoundManager;
    virtual ~Sound();

    void play();
    void pause();
    void stop();

    bool isPlaying();
    bool isLoop();

    float getVolume() const;
    void setVolume(float volume);

private:
    explicit Sound(std::string_view filename, bool isLoop, const SoundParams& params);

    struct Buffer
    {
        uint8_t *start = nullptr;
        size_t size = 0;
        std::atomic<size_t> current_pos = 0;
    } _buffer;

    enum State
    {
        Playing,
        Paused
    };
    std::atomic<State> _state = State::Paused;

    std::atomic<bool> _isLoop;

    std::atomic<float> _volume = 1.0;
};


#endif //GAME_SOUND_HPP
