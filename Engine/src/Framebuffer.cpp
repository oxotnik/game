//
// Created by Андрей Марцинкевич on 3.01.22.
//

#include "Framebuffer.hpp"

std::shared_ptr<Texture> Framebuffer::getTexture() const
{
    return _texture;
}
