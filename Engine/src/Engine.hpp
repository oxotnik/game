//
// Created by Андрей Марцинкевич on 17.11.21.
//

#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <memory>
#include <string_view>
#include <glm/glm.hpp>

class Window;
class Renderer;
class EventsManager;
class VertexBuffer;
class Node;
class ScheduleManager;
class SoundManager;
class ImguiManager;
class ActionManager;
class FileManager;

class Engine
{
public:
    explicit Engine();
    ~Engine();

    void init(std::string_view windows_name, size_t width, size_t height);

    bool isActive();
    void update();

    glm::vec2 getVirtualResolution() const;
    void setVirtualResolution(glm::vec2 virtualResolution) const;

    [[nodiscard]] const EventsManager& eventsManager() const;
    [[nodiscard]] const Renderer& renderer() const;
    [[nodiscard]] const Window& window() const;
    [[nodiscard]] const ScheduleManager& scheduleManager() const;
    [[nodiscard]] const SoundManager& soundManager() const;
    [[nodiscard]] const ActionManager& actionManager() const;
    [[nodiscard]] const FileManager& fileManager() const;

    std::shared_ptr<Node> scene();

    template<typename T, typename... Args>
    std::shared_ptr<T> createShared(Args&&... args)
    {
        if constexpr(std::is_constructible_v<T, Engine&, Args&&...>)
        {
            return std::make_shared<T>(*this, std::forward<Args>(args)...);
        }
        else
        {
            return std::make_shared<T>(std::forward<Args>(args)...);
        }
    }

    template<typename T, typename... Args>
    std::unique_ptr<T> createUnique(Args&&... args)
    {
        if constexpr(std::is_constructible_v<T, Engine&, Args&&...>)
        {
            return std::make_unique<T>(*this, std::forward<Args>(args)...);
        }
        else
        {
            return std::make_unique<T>(std::forward<Args>(args)...);
        }
    }

    template<typename T, typename... Args>
    std::shared_ptr<T> createShared(Args&&... args) const
    {
        if constexpr(std::is_constructible_v<T, const Engine&, Args&&...>)
        {
            return std::make_shared<T>(*this, std::forward<Args>(args)...);
        }
        else
        {
            return std::make_shared<T>(std::forward<Args>(args)...);
        }
    }

    template<typename T, typename... Args>
    std::unique_ptr<T> createUnique(Args&&... args) const
    {
        if constexpr(std::is_constructible_v<T, const Engine&, Args&&...>)
        {
            return std::make_unique<T>(*this, std::forward<Args>(args)...);
        }
        else
        {
            return std::make_unique<T>(std::forward<Args>(args)...);
        }
    }

    std::shared_ptr<Node> getCamera() const;

private:
    std::unique_ptr<Window> _window;
    std::unique_ptr<Renderer> _renderer;
    std::unique_ptr<EventsManager> _eventsManager;
    std::unique_ptr<ScheduleManager> _scheduleManager;
    std::unique_ptr<SoundManager> _soundManager;
    std::unique_ptr<ImguiManager> _imguiManager;
    std::unique_ptr<ActionManager> _actionManager;
    std::unique_ptr<FileManager> _fileManager;


    bool _isActive = false;

    std::shared_ptr<Node> _scene;
    std::shared_ptr<Node> _camera;

    mutable glm::vec2 _virtualResolution;
};


#endif
