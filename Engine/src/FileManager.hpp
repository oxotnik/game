//
// Created by Андрей Марцинкевич on 12.01.22.
//

#ifndef GAME_FILEMANAGER_HPP
#define GAME_FILEMANAGER_HPP

#include <string>
#include <string_view>
#include <vector>

class FileManager
{
public:
    [[nodiscard]] std::string resourceLocation(std::string filename) const;

    void setFindPaths(std::vector<std::string> findPaths);
    void forEachPath(const std::function<void(std::string)>& callback) const;

private:
    std::vector<std::string> _findPaths;
};


#endif //GAME_FILEMANAGER_HPP
