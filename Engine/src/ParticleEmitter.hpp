//
// Created by Андрей Марцинкевич on 22.12.21.
//

#ifndef GAME_PARTICLEEMITTER_HPP
#define GAME_PARTICLEEMITTER_HPP


#include "Node.hpp"
#include <Renderer.hpp>

class TextureUniform;
class Vec2Uniform;
class FloatUniform;
class Mat3Uniform;

class ParticleEmitter : public Node
{
public:
    ParticleEmitter(const Engine &engine);

    void start(size_t count);
    void start(size_t count, float freq, float lifeTime);

    void update(fseconds dt) override;

protected:
    void visitSelf() override;

private:
    Renderer::Command _command;

    std::shared_ptr<Vec2Uniform> _screenSizeUniform;
    std::shared_ptr<Vec2Uniform> _resolutionUniform;
    std::shared_ptr<FloatUniform>  _timeUniform;
    std::shared_ptr<Mat3Uniform>  _cameraUniform;

    bool _dynamicMode = false;
    float _freq = 0;
    float _lifeTime = 0;
    float _lastGenTime = 0;
    float _totalTime = 0;
    float _count = 0;
};


#endif //GAME_PARTICLEEMITTER_HPP
