#include <Engine.hpp>
#include <Renderer.hpp>
#include <Sprite.hpp>
#include <Window.hpp>
#include <chrono>
#include <thread>

#include "Tank.hpp"
#include "Sound.hpp"
#include "SoundManager.hpp"
#include "ParticleEmitter.hpp"
#include "ActionManager.hpp"


int main()
{
    Engine engine{};
    engine.init("Hello, world!", 1280, 720);
    engine.setVirtualResolution(glm::vec2{1920.0f, 1080.0f});

    const auto& eventManager = engine.eventsManager();

    int tiledMap[15][20] = {
            {0,0,0,0,0,0,0,0,0, 0,0, 4, 0, 0, 0, 0, 0, 0, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 2, 1, 1, 7, 0, 0, 0, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 0, 0},
            {1,1,1,1,1,1,1,1,1, 1,1, 1, 1, 1, 3, 1, 1, 7, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
            {0,0,0,0,0,0,0,0,0, 0,0, 0, 0, 0, 4, 0, 0, 4, 0},
    };

    std::string tiles[] =
            {
                "roadPLAZA.tga", //0
                "roadEW.tga", //1
                "roadNE.tga", //2
                "roadNEWS.tga", //3
                "roadNS.tga", //4
                "roadNW.tga", //5
                "roadSE.tga", //6
                "roadSW.tga"  //7

            };

    for (int i = 0; i < 20; ++i)
    {
        for (int j = 0; j < 15; ++j)
        {
            auto tiled = engine.createShared<Sprite>(tiles[tiledMap[j][i]]);
            tiled->setAnchor(glm::vec2{0.0, 0.0});
            tiled->setPosition(tiled->getContentSize() * glm::vec2{i, j});
            engine.scene()->addNode(tiled);
        }
    }

    auto sound = engine.soundManager().createSound("music.wav", true);
    sound->play();
    sound->setVolume(0.2);

    auto tank = engine.createShared<Tank>();
    tank->setScale(glm::vec2(1.0f));
    tank->setPosition(glm::vec2(engine.window().getWidth(),
                                  engine.window().getHeight()) * 0.5f);
    engine.scene()->addNode(tank);

    while (engine.isActive())
    {
        auto delta = tank->getPosition() - engine.getCamera()->getPosition();
        engine.getCamera()->setPosition(engine.getCamera()->getPosition() + delta * 4.0f * engine.scheduleManager().getCurrentDelta().count());

        engine.getCamera()->setScale(glm::vec2(0.5f + std::abs(0.00098f * tank->getSpeed())));
        engine.update();
    }

    return 0;
}
