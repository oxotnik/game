//
// Created by Андрей Марцинкевич on 2.12.21.
//

#ifndef GAME_TANK_HPP
#define GAME_TANK_HPP

#include <Node.hpp>
#include <EventsManager.hpp>


class Engine;
class Sound;
class Tank : public Node
{
public:
    explicit Tank(Engine&);

    float getSpeed() const;

private:
    std::shared_ptr<Node> _body;
    std::shared_ptr<Node> _barrel;

    bool _isBarrelLeft = false;
    bool _isBarrelRight = false;

    bool _isLeft = false;
    bool _isRight = false;
    bool _isUp = false;
    bool _isDown = false;

    float _speed = 0;
    float _turnSpeed = 0;

    std::shared_ptr<Sound> _shootSound;

public:
    void update(fseconds dt) override;

};


#endif //GAME_TANK_HPP
